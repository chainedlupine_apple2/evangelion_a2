.include "zeropage.inc"

.segment "ZEROPAGE"

  ; arguments for subroutines
  arg1  = $60
  arg2  = $61
  arg3  = $62
  arg4  = $63
  arg5  = $64
  arg6  = $65

  arg1w = $66
  arg2w = $68
  arg3w = $6A
  arg4w = $6C
  arg5w = $6E

  ; for use in subroutines
  temp1 = $70
  temp2 = $71
  temp3 = $72
  temp4 = $73
  temp5 = $74
  temp6 = $75

  temp1w  = $76
  temp2w  = $78
  temp3w  = $7A
  temp4w  = $7C
  temp5w  = $7E

    
  ; intended for variables in main program
  ;var1	:= $80
  ;var2	:= $81
  ;var3	:= $82
  ;var4	:= $83
  ;var5	:= $84
  ;var6	:= $85

  ;var1w	:= $86
  ;var2w	:= $88
  ;var3w	:= $8A
  ;var4w	:= $8C
  ;var5w	:= $8E



.segment "CODE_END_H"

.include "apple2rom.inc"

.export zpsave_addr_l, zpsave_addr_h, _ZP_SAVE, _ZP_RESTORE, _ZP_SETUP

zpsave_addr_l  = $5e
zpsave_addr_h  = $5f

.import __DATA_ZPSCRATCH_LOAD__

.macro ZP_SAVE_INC
    inc   zpsave_addr_h
.endmacro

.macro ZP_SAVE_DEC
    dec   zpsave_addr_h
.endmacro

_ZP_SETUP:
    lda   #<__DATA_ZPSCRATCH_LOAD__
    sta   zpsave_addr_l
    lda   #>__DATA_ZPSCRATCH_LOAD__
    sta   zpsave_addr_h
    rts


_ZP_SAVE:
    php
    pha
    U_PHY
    ldy   #temp5w+1
  :
    lda   $00,y
    sta   (zpsave_addr_l),y
    dey
    cpy   #arg1
    bcs   :-

    ZP_SAVE_INC
    U_PLY
    pla
    plp
    rts


_ZP_RESTORE:
    php
    pha
    U_PHY
    ZP_SAVE_DEC

    ldy   #temp5w+1
  :
    lda   (zpsave_addr_l),y
    sta   $00,y
    dey
    cpy   #arg1
    bcs   :-

    U_PLY
    pla
    plp
    rts

.segment "DATA_ZPSCRATCH"
  ; empty, just here to make ca65 generate a reference to it

