.include "utils.inc"

.scope ROM
.include "apple2rom-defs.inc"
.endscope

.segment "CODE"

; JUMP TABLE at beginning of code
entry_clrhgr:
    jmp clrhgr      ; CALL 768
entry_slowfade:
    jmp slowfade    ; CALL 771
; END JUMP TABLE

SUB clrhgr
    ; performs mem fill (in same bank), uses ZP A1/A2/A3
    ; A1 = dst
    ; A2 = value (low only)
    ; A3 = size

    lda   #$00
    sta   ROM::A1L
    lda   #$20
    sta   ROM::A1H

    ; outer loop
    ldy   #0
    ldx   #$20  ; A3H
    
    cli
  @HCOPY:
    lda   #0
    .repeat 4
    sta   (ROM::A1L), Y
    iny
    .endrepeat
    
    bne   @HCOPY
    
    inc   ROM::A1H
    dex
    bne   @HCOPY
    
  @DONE:
    sei
    rts
END_SUB


SUB slowfade
    lda   #$00
    sta   ROM::A1L
    lda   #$20
    sta   ROM::A1H

    ; outer loop
    ldy   #0
    
    ldx   #$20  ; A3H
    cli
    
  @HCOPY:
    lda   #0
    .repeat 40
    sta   (ROM::A1L), Y
    .endrepeat
    iny
    bne   @HCOPY
    
    inc   ROM::A1H
    dex
    bne @HCOPY

  @DONE:
    sei

    rts
END_SUB