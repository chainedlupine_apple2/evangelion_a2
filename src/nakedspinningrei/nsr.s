.segment "CODE_H"

.include "../zeropage.inc"
.include "utils.inc"
.include "apple2rom.inc"

.include "../../buzzbox_a2/src/buzzbox/sound.inc"
.include "../../buzzbox_a2/src/buzzbox/sound-music.inc"
.include "../../buzzbox_a2/src/buzzbox/fileio.inc"

.include "hgr-table.inc"

.include "nsr.inc"


.segment "CODE_H"


SUB nsr_startup
    ROM_MEMFILL #ROM::MEM_HGR_PAGE1, #0, #$2000

    ; copy eva text over to page 1
    jsr   copy_logo

    jsr   ROM::GFX_HGR_PAGE1
    jsr   ROM::GFX_MODE_HGR

    ;jsr ROM::ROM_GETKEY
    lda   #0
    ldx   #6
    jsr   delay_keypress

    jsr   ROM::TEXT_MODE_40

    ; copy to moon to page 2
    ROM_MEMCOPY #ROM::MEM_HGR_PAGE2, #IMG_MOON, #$2000

    ; then copy second moon glint over to page 2
    jsr   copy_moon_glint

    ; just copy to page 1
    ROM_MEMCOPY #ROM::MEM_HGR_PAGE1, #IMG_MOON, #$2000

    jsr   ROM::GFX_HGR_PAGE1
    jsr   ROM::GFX_MODE_HGR

    lda   #0
    sta   curr_page

    rts
END_SUB

SUB slow_fadeout
    lda   curr_page
    bne   :+
      SLOW_MEMFILL #ROM::MEM_HGR_PAGE1, #0, #$2000
      jmp skip
  :
    SLOW_MEMFILL #ROM::MEM_HGR_PAGE2, #0, #$2000
  skip:
    rts
END_SUB

SUB delay_keypress
    sta   temp1w
    stx   temp1w+1

    sta   ROM::SB_KEYB
  key_loop:
    ; first check for keypress
    lda   ROM::IN_KEYB
    bmi   exit

    lda   #50
    ldx   #1
    jsr   ROM::ROM_WAIT

    Util_Dec_16_Addr    temp1w, #1
    Util_16_Addr_IsZero temp1w

    bcc   key_loop

  exit:
    sta   ROM::SB_KEYB

    rts
END_SUB

SUB nsr_main

  loop:
    Util_LOAD   #BBT_FLYME1_SEQS, arg1w
    Util_LOAD   #music_callback, arg2w

    lda   #1
    sta   ROM::SB_KEYB  ; clear keyboard strobe
    jsr   PLAY_SEQ_PTRS

    lda   abort_flag
    beq   loop

    rts
END_SUB

SUB copy_logo
    var_src         = temp1w
    var_dst         = temp2w
    line            = temp5

    lda   #0
    sta   line


  line_loop:
    lda   #<IMG_ANIM
    sta   var_src
    lda   #>IMG_ANIM
    sta   var_src+1

    lda   line
    clc
    adc   #128
    tax

    lda   HGR_Y_ADDR_LOOKUP_L, x
    sta   temp1
    Util_Inc_16_Addr var_src, temp1

    lda   DATA_Y_ADDR_LOOKUP_H, x
    sta   temp1
    
    lda   var_src+1
    clc
    adc   temp1
    sta   var_src+1

    lda   line
    clc
    adc   #60
    tax

    lda HGR_Y_ADDR_LOOKUP_L, x
    sta   var_dst

    lda HGR_Y_ADDR_LOOKUP_H, x
    sta   var_dst+1

    Util_Inc_16_Addr var_src, #20

    Util_Inc_16_Addr var_dst, #10

    ldy   #0

  .repeat 10
    lda   (var_src), y
    sta   (var_dst), y
    iny
    lda   (var_src), y
    sta   (var_dst), y
    iny
  .endrepeat


    inc   line
    lda   line
    cmp   #64
    bcs   :+
    jmp   line_loop
  :

    rts

END_SUB

SUB copy_moon_glint
    var_src = temp1w
    var_dst = temp2w
    line    = temp5

    lda   #0
    sta   line


  line_loop:
    lda   #<IMG_ANIM
    sta   var_src
    lda   #>IMG_ANIM
    sta   var_src+1

    lda   line
    clc
    adc   #128
    tax

    lda   HGR_Y_ADDR_LOOKUP_L, x
    sta   temp1
    Util_Inc_16_Addr var_src, temp1

    lda   DATA_Y_ADDR_LOOKUP_H, x
    sta   temp1
    
    lda   var_src+1
    clc
    adc   temp1
    sta   var_src+1

    ldx   line

    lda   HGR_Y_ADDR_LOOKUP_L, x
    sta   var_dst

    lda   HGR_Y_ADDR_LOOKUP_H_PAGE2, x
    sta   var_dst+1


    Util_Inc_16_Addr var_dst, #10

    ldy   #0

  .repeat 9
    lda   (var_src), y
    sta   (var_dst), y
    iny
    lda   (var_src), y
    sta   (var_dst), y
    iny
  .endrepeat


    inc   line
    lda   line
    cmp   #64
    bcs   :+
    jmp   line_loop
  :

    rts

END_SUB


SUB anim
    var_loop        = temp5
    var_hgr_offset  = temp4
    var_src         = temp1w
    var_dst         = temp2w
    cli
    ZP_SAVE

    lda   curr_page
    bne   :+
      ; on page 1, so draw on page 2
      lda   #<HGR_Y_ADDR_LOOKUP_H_PAGE2
      sta   page_patch+1
      lda   #>HGR_Y_ADDR_LOOKUP_H_PAGE2
      sta   page_patch+2
      jmp   skip_patch
  :
    ; on page 2, so draw on page 1
    lda   #<HGR_Y_ADDR_LOOKUP_H
    sta   page_patch+1
    lda   #>HGR_Y_ADDR_LOOKUP_H
    sta   page_patch+2

  skip_patch:
    lda   #30
    sta   var_hgr_offset

    lda   #0
    sta   var_loop
  line_loop:
    lda   #<IMG_ANIM
    sta   var_src
    lda   #>IMG_ANIM
    sta   var_src+1

    ; first handle rei
    ldx   working_line

    lda   HGR_Y_ADDR_LOOKUP_L, x
    sta   temp1
    Util_Inc_16_Addr var_src, temp1

    lda   DATA_Y_ADDR_LOOKUP_H, x
    sta   temp1
    
    lda   var_src+1
    clc
    adc   temp1
    sta   var_src+1

    lda   HGR_Y_ADDR_LOOKUP_L, x
    sta   var_dst

  page_patch:
    lda   HGR_Y_ADDR_LOOKUP_H, x
    sta   var_dst+1

    lda   rei_frame
    asl
    asl
    asl   ;8x
    sta   temp1
    Util_Inc_16_Addr temp1w, temp1

    Util_Inc_16_Addr var_dst, var_hgr_offset

    ldy   #0

    .repeat 8
    lda   (var_src), y
    sta   (var_dst), y
    iny
    .endrepeat

    ;Util_Inc_16_Addr temp2w, #40

    inc   working_line
    lda   working_line
    cmp   #128
    bcc   skip_reset
    ; it's time to start a new frame
    ; first, flip pages
    jsr   flip_page
    lda   #0
    sta   working_line
    inc   rei_frame
    lda   rei_frame
    cmp   #5 ; reset after 5 frames
    bcc   :+
      lda   #0
      sta   rei_frame
  :
    jmp   done

  skip_reset:
    inc   var_loop
    lda   var_loop
    cmp   #60 ; stop drawing after 60 lines
    bcs   done
    jmp   line_loop

  done:
    ZP_RESTORE
    sei
    rts
END_SUB

; 28 pixels by 128 pixels for Rei

SUB music_callback
    inc   callback_tick
    lda   callback_tick
    cmp   #2
    bcc   skip_tick
      lda   #0
      sta   callback_tick

      ; first check keypress
      lda   ROM::IN_KEYB
      bpl   :++
        ; key press detected
        ; now check to see if it is ESC
        and   #$7F
        cmp   #ROM::KB::kb_escape
        bne   :+
          lda   #1
          sta   abort_flag
          sec   ; set carry to force music player to exit
          rts
      :
        sta   ROM::SB_KEYB  ; clear strobe for next keypress
    :

      ; do anim tick
      jsr   anim

  skip_tick:
    clc
    rts
END_SUB

SUB flip_page
    lda   curr_page
    bne   :+
      ; on page 1, flip to 2
      jsr   ROM::GFX_HGR_PAGE2
      lda   #1
      jmp   skip
  :
    ; on page 2, flip to 1
    jsr   ROM::GFX_HGR_PAGE1
    lda   #0
  skip:
    sta   curr_page

    rts
END_SUB


_SLOW_MEMFILL:
    ; outer loop
    ldy #0
    
    ldx	ROM::A3H
    beq @LREST
    
  @HCOPY:
    lda	ROM::A2L
    .repeat 40
    sta (ROM::A1L), Y
    .endrepeat
    iny
    
    bne @HCOPY
    
    inc ROM::A2H
    inc ROM::A1H
    dex
    bne @HCOPY
    
  @LREST:
    ldx ROM::A3L
    beq @DONE
    
  @LCOPY:
    lda ROM::A2L
    .repeat 40
    sta (ROM::A1L), Y
    .endrepeat
    iny
    dex
    bne @LCOPY
    
  @DONE:
    RTS



.segment "DATA_H"

DEFDATA callback_tick
  .byte 0
END_DEFDATA

; rei anim uses 0-128, and wavey uses 128-192
DEFDATA working_line
  .byte $00
END_DEFDATA

DEFDATA rei_frame
  .byte $1
END_DEFDATA

DEFDATA curr_page
  .byte $00
END_DEFDATA

DEFDATA abort_flag
  .byte $00
END_DEFDATA