.segment "CODE_H"

jmp MAIN

.include "utils.inc"
.include "apple2rom.inc"
.include "../zeropage.inc"
.include "prodos.inc"
.include "../../buzzbox_a2/src/buzzbox/fileio.inc"

.global nsr_startup, nsr_main, slow_fadeout

SUB MAIN
    ; reset stack
    ldx   #$FF
    txs

    ;JSR CHECK_FOR_BASIC
    ZP_SETUP

    ; initial ProDOS SYSTEM setup
    jsr   START_FROM_PRODOS
    
    jsr   ROM::TEXT_MODE_40
    jsr   ROM::ROM_HOME ; home, clrscreen

    jsr   nsr_startup

    jsr   nsr_main

    ; clear memory
    jsr   slow_fadeout

    jsr   ROM::TEXT_MODE_40

    jsr   QUIT_TO_BASIC_SYSTEM
END_SUB

START_FROM_PRODOS:
    jsr   ProDOS::RAMDISK_DISCONNECT
    RTS

.define BASIC_SYSTEM_STR "BASIC.SYSTEM"


SUB QUIT_TO_BASIC_SYSTEM
    lda   ROM::SW_LCBANK2RW			; swap in bank 2
    lda   #$D8 	; CLD
    sta   $D100	; Perform our selector instead of ProDOS dispatcher

    ; write BASIC.SYSTEM to $280
    ldx   #.strlen(BASIC_SYSTEM_STR)
    stx   $0280
  cloop:
    lda   basic_system_name, x
    sta   $0280, x
    dex
    bne   cloop

    lda   #$00
    ldx   #$30
    jsr   ProDOS::RAMDISK_SET_FORMAT_BUFFER
    jsr   ProDOS::RAMDISK_RECONNECT

    ProDOS_QUIT (quit_param)
    ;jmp    ROM::ROM_PWRUP		; fugly, but I don't give a shit
END_SUB

SUB QUIT_TO_PRODOS
    lda   #$00
    ldx   #$30
    jsr   ProDOS::RAMDISK_SET_FORMAT_BUFFER
    jsr   ProDOS::RAMDISK_RECONNECT

    ProDOS_QUIT (quit_param)
END_SUB

.segment "RODATA_H"

basic_system_name:
    .byte .strlen(BASIC_SYSTEM_STR)
    .byte BASIC_SYSTEM_STR

quit_param:
    ProDOS_DEFINE_QUIT_PARAM 
