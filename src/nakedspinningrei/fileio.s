.include "apple2rom.inc"
.include "apple2rom-defs.inc"
.include "prodos.inc"
.include "utils.inc"
.include "fileio.inc"
.include "../zeropage.inc"

.segment "CODE_END_H"

    ; arg1w = addr to filename path (as LPSTR)
    ; return = no carry if successful, otherwise error in accum
SUB FILE_OPEN
    ; ------- attempt to load a file into some buffers -----
    Util_LOAD arg1w, fileio_open_param+ProDOS::STRUCT_OPEN_PARAM::PATHNAME
    
    ProDOS_OPEN fileio_open_param
    rts
END_SUB

; copy the open file REF_NUM over to all other structs, so they will work
SUB FILE_PREPARE
    lda   fileio_open_param+ProDOS::STRUCT_OPEN_PARAM::REF_NUM		
    sta   fileio_read_param+ProDOS::STRUCT_READ_PARAM::REF_NUM
    sta   fileio_write_param+ProDOS::STRUCT_READ_PARAM::REF_NUM
    sta   fileio_close_param+ProDOS::STRUCT_CLOSE_PARAM::REF_NUM

    rts
END_SUB

; arg1w = addr to filename (LPSTR)
; return: carry if error, error in accum
SUB FILE_CREATE
    Util_LOAD arg1w, fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::PATHNAME

    lda   #$C3 ; destroy, rename, write, read enabled
    sta   fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::ACCESS

    lda   #$7F ; general binary file
    sta   fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::FILE_TYPE

    lda   #$00
    sta   fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::AUX_TYPE
    sta   fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::AUX_TYPE+1
    sta   fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::CREATE_DATE
    sta   fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::CREATE_DATE+1
    sta   fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::CREATE_TIME
    sta   fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::CREATE_TIME+1

    lda   #$01 ; standard file
    sta   fileio_create_param+ProDOS::STRUCT_CREATE_PARAM::STORAGE_TYPE

    ProDOS_CREATE fileio_create_param
    rts
END_SUB

SUB FILE_CLOSE
    ProDOS_CLOSE fileio_close_param
    rts
END_SUB

; arg1w = buffer to write
; arg2w = size of data to write
SUB FILE_WRITE
    Util_LOAD arg1w, fileio_write_param+ProDOS::STRUCT_WRITE_PARAM::DATA_BUFFER
    Util_LOAD arg2w, fileio_write_param+ProDOS::STRUCT_WRITE_PARAM::REQUEST_COUNT

    ProDOS_WRITE fileio_write_param
    bcs   write_error

    ; verify we wrote the amount requested
    lda   arg2w
    cmp   fileio_write_param+ProDOS::STRUCT_WRITE_PARAM::TRANS_COUNT
    bne   :+
    lda   arg2w+1
    cmp   fileio_write_param+ProDOS::STRUCT_WRITE_PARAM::TRANS_COUNT+1
    bne   :+
      lda   #0 ; all good
      clc
      rts
  : ; error on write
    lda   #ProDOS::PRODOS_ERR_OVERRUN
    sec
  write_error:
    ; error, just straight outta here
    rts
END_SUB

; arg1w = buffer to read into
; arg2w = size of data to read
SUB FILE_READ
    Util_LOAD arg1w, fileio_read_param+ProDOS::STRUCT_READ_PARAM::DATA_BUFFER
    Util_LOAD arg2w, fileio_read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT

    ProDOS_READ fileio_read_param
    bcs   read_error

    ; verify we read the amount requested
    lda   arg2w
    cmp   fileio_read_param+ProDOS::STRUCT_READ_PARAM::TRANS_COUNT
    bne   :+
    lda   arg2w+1
    cmp   fileio_read_param+ProDOS::STRUCT_READ_PARAM::TRANS_COUNT+1
    bne   :+
      lda   #0 ; all good
      clc
      rts
  : ; error on read
    lda   #ProDOS::PRODOS_ERR_EOF
    sec

  read_error:
    ; error, just leave
    rts

END_SUB

; A = ProDOS error code
SUB EXIT_PRODOSERROR
    pha
    jsr ROM::TEXT_MODE_40
    ROM_PRINT #error_file_msg
    pla
    jsr   ROM::ROM_PRBYTE
    jsr   ROM::ROM_GETKEY ; GETKEY

    jmp   QUIT_TO_PRODOS
END_SUB

; arg1w = path to delete
SUB FILE_DESTROY
    Util_LOAD arg1w, fileio_destroy_param+ProDOS::STRUCT_DESTROY_PARAM::PATHNAME
    ProDOS_DESTROY fileio_destroy_param

    rts
END_SUB

; arg1w = prefix to set (this also sets boot_volname)
SUB FILE_SET_PREFIX
    Util_LOAD arg1w, fileio_getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
    ProDOS_SET_PREFIX fileio_getset_prefix_param
    rts
END_SUB


SUB FILE_GET_BOOT_UNIT_NUM
    lda   #0
    sta   arg1
    jsr   FILE_GET_BOOT_INFO
    rts
END_SUB

SUB FILE_GET_BOOT_UNIT_NUM_AND_VOLNAME
    lda   #1
    sta   arg1
    jsr   FILE_GET_BOOT_INFO
    rts
END_SUB

; arg1 = if >0, then set the fileio_boot_volname from ON_LINE record
SUB FILE_GET_BOOT_INFO
    volname_write   = arg1

    ; store our current prefix in self half of io_buffer
    Util_LOAD #fileio_buffer+$200, fileio_getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
    ProDOS_GET_PREFIX fileio_getset_prefix_param
    bcc   :+
      rts
  :

    ; force all devices to rescan their ProDOS volumes
    Util_LOAD #fileio_buffer, fileio_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
    Util_LOAD #$00, fileio_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::UNIT_NUM
    ProDOS_ON_LINE fileio_on_line_param
    bcc   :+
      rts
  :

    ; now scan online records, looking for the prefix that we booted with
    Util_LOAD #fileio_buffer, temp1w
    Util_LOAD #fileio_buffer+$201, temp2w	; skip one bytes, because first byte of GET_PREFIX is len of string, and second is always / ($2F)

    ; store error'd boot unit number for now		
    lda   #$ff
    sta   fileio_boot_unitnum

    lda   #0
    sta   temp5   ; holds current record idx count
  @loop_record:
    ldy   #0
    lda   (temp1w),y
    tax   ; save first byte for now
    and   #$F0  ; strip off the length
    bne   :+
      ; unit is zero, so we're at end
      jmp @done
  :
    sta   temp3 ; store unit number temporarily for now
    txa   ; restore so we can check disk/slot #
    and   #$0F	; strip off the disk/slot
    bne   :+
      ; len=0, so this must be errored device, skip
      jmp   @next_record
  :
    sta   temp4 ; store length temporarily too
    tay   ; store length in y
    ; now, let's compare the prefix to the one we saved
  @loop_char:
    lda   (temp1w),y
    sta   temp6
    lda   (temp2w),y
    cmp   temp6
    bne   @next_record  ; didn't match, so skip
    dey
    bne   @loop_char
    ; no mismatch found, so we must match
    
  @match_found:
    ; write boot number from temp store
    lda   temp3 
    sta   fileio_boot_unitnum

    lda   volname_write
    beq   @done

    ; now write volume name to boot_volname
    ldy   temp4 ; write length to first byte
    iny   ; increment by one, because we're adding / at beginning
    iny   ; increment by another, as we're adding / at the end too
    sty   fileio_boot_volname

    lda   #$2F
    sta   fileio_boot_volname+1  ; write / ($2F) to first character
    sta   fileio_boot_volname, y ; write / to end

    ; now copy over name
    dey
    dey
  @copy_char:
    lda   (temp1w),y
    sta   fileio_boot_volname+1,y
    dey
    bne   @copy_char

    jmp   @done

  @next_record:
      Util_Inc_16_Addr temp1w, #$10
      inc   temp5  ; increase record index count
      lda   temp5
      cmp   #$0F   ; check to see if count is >15, if so loop again
      bcc   @loop_record

      ; all records exhausted.
      ; and no matches found...

  @done:
      clc
      rts
END_SUB

    ; forces all devices to rescan their ProDOS volumes
SUB FILE_DISK_ALL_ON_LINE
    ; we don't care about results, just us io_buffer
    Util_LOAD #fileio_buffer, fileio_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
    ProDOS_ON_LINE fileio_on_line_param
    rts
END_SUB


SUB FILE_DISPLAY_UNITNUM
    rts
END_SUB

    ; reloads the volume name found in the boot drive (usually slot 6, drive 1)
    ; it then attempts to set our current prefix to that
SUB FILE_RESYNC_VOLUME_NAME
    ; force just our boot device to rescan its ProDOS volume
    Util_LOAD #fileio_buffer, fileio_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
    lda   fileio_boot_unitnum
    cmp   #$ff
    bne   :+
    jmp   @vol_name_error
  :
    sta   fileio_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::UNIT_NUM
    ProDOS_ON_LINE fileio_on_line_param
    bcc   :+
      ; otherwise online error
      pha   ; save error
      ldx   #0
      lda   #0
    @clearerr:
      sta   fileio_buffer+$200,x
      inx
      cpx   #$80
      bcc   @clearerr

      pla
      sec
      rts
    :

    ; first check for error
    lda   fileio_buffer
    and   #$F0 ; mask off last 4 bits
    bne   :+
      ; zero, so error
      lda   fileio_buffer+1
      sec
      rts
    : ; if not zero, then we're fine
    
    ; copy it over to io_buffer+512 bytes, turning it into a set_prefix style name in the process
    lda   #$2F
    sta   fileio_buffer+$201 ; first write $2F to io_buffer+$201

    ; have to do this by adding one to the length and preceeding it with a / ($2F)
    lda   fileio_buffer
    ; mask off unit_num
    and   #$0F
    beq   @vol_name_error

    U_INC ; add one to length
    sta   fileio_buffer+$200  ; store at beginning of buffer
    U_DEC ; return to normal
    tax
  @loop_char:
    lda   fileio_buffer,x
    sta   fileio_buffer+$201,x
    dex
    bne   @loop_char

    ; now our new volume prefix should be at io_buffer+$200

    ; now set our new prefix
    Util_LOAD #fileio_buffer+$200, fileio_getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
    ProDOS_SET_PREFIX fileio_getset_prefix_param

    jmp   @done

  @vol_name_error:
    ; something wrong w/ volume, just return
    lda   #$FF
    sec
    rts

  @done:
    clc
    rts

END_SUB

SUB FILE_TEMPVOLNAME_TO_BOOTVOLNAME
    Util_LOAD #fileio_buffer, arg1w
    inc   arg1w+1
    inc   arg1w+1
    jsr   FILE_LSTR_TO_BOOTVOLNAME
    rts
END_SUB

; arg1w = source to copy (LSTR)
SUB FILE_LSTR_TO_BOOTVOLNAME
    templstr = arg1w

    ldx   #0
    lda   #0
  clear_loop:
    sta   fileio_boot_volname, x
    inx
    cpx   #$40
    bcc   clear_loop

    ; check length
    ldy   #0
    lda   (templstr), y
    bne   :+
      ; error, so let's set this to just a slash
      lda   #1
      sta   fileio_boot_volname
      lda   #$2F
      sta   fileio_boot_volname+1  
      rts
  :

    ldx   #0
    ldy   #0
    lda   (templstr), y
    sta   temp1
    ldy   #1
  loop_char:
    lda   (templstr), y
    sta   fileio_boot_volname+1,x
    iny
    inx
    cpx   temp1
    bne   loop_char

    ; add a / to the end
    inx
    lda   #$2F
    sta   fileio_boot_volname,x

    ; copy over the original size
    lda   temp1
    sta   fileio_boot_volname

    ; increment first character to indicate 1+ path
    inc   fileio_boot_volname

    rts
END_SUB

dir_load_size := $0200

; arg1w = buffer to use for directory file
; arg2w = routine to call with the file entry data
SUB FILE_SHOW_CATALOG
    dirBlock        = arg1w
    callback        = arg2w
    entryPtr        = temp1w
    fileCount       = temp2w
    activeEntries   = temp3w
    entryLength     = temp1
    entriesPerBlock = temp2
    blockEntries    = temp3

    lda   callback
    sta   patchup_callback+1
    lda   callback+1
    sta   patchup_callback+2


    Util_LOAD #fileio_boot_volname, fileio_open_param+ProDOS::STRUCT_OPEN_PARAM::PATHNAME		
    ProDOS_OPEN fileio_open_param
    bcc   :+
      rts
  :

    jsr   FILE_PREPARE

    ; read first 512 bytes
    Util_LOAD arg1w, fileio_read_param+ProDOS::STRUCT_READ_PARAM::DATA_BUFFER
    lda   #<dir_load_size
    sta   fileio_read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT
    lda   #>dir_load_size
    sta   fileio_read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT+1

    ProDOS_READ fileio_read_param
    bcc   :+
      rts
  :

    ; alright, start the scan
    ldy   #$23
    lda   (dirBlock), y
    sta   entryLength

    ldy   #$24
    lda   (dirBlock), y
    sta   entriesPerBlock

    ldy   #$25
    lda   (dirBlock), y
    sta   fileCount

    ldy   #$26
    lda   (dirBlock), y
    sta   fileCount+1

    Util_LOAD dirBlock, entryPtr

    ; begin scan past directory header
    lda   entryLength
    clc
    adc   #$4
    sta   temp4
    Util_Inc_16_Addr entryPtr, temp4

    lda   #2
    sta   blockEntries

    lda   #0
    sta   activeEntries
    sta   activeEntries+1

  dirloop:
    ldy   #0
    lda   (entryPtr), y
    ; if zero, skip
    beq   invalidEntry
      ; process entry by printing it out
      ZP_SAVE
      Util_LOAD entryPtr, arg1w
    patchup_callback:
      jsr   $0000
      ZP_RESTORE

      Util_Inc_16_Addr activeEntries, #1
  invalidEntry:

    ; 16-unsigned compare on activeEntries < fileCount
    lda   activeEntries+1
    cmp   fileCount+1
    bcs   :+
      jmp   activeLessThan
    :
    bne   fileGreaterThan
    lda   activeEntries
    cmp   fileCount
    bcs   :+
      jmp   activeLessThan
  :
    jmp   fileGreaterThan
  activeLessThan:
    ; activeEntries < fileCount, so more entries to process
    lda   blockEntries
    cmp   entriesPerBlock
    beq   loadNextBlock
      ; we are not done, so continue next entry
      Util_Inc_16_Addr entryPtr, entryLength
      inc   blockEntries
      jmp   dirloop
  loadNextBlock:
    ; our current block is done, so read next
    ProDOS_READ fileio_read_param
    bcc   :+
      rts
  :
    ; reset entryPtr back to top of block, then skip 4 bytes
    Util_LOAD dirBlock, entryPtr
    lda   #$4
    sta   temp4
    Util_Inc_16_Addr entryPtr, temp4

    lda   #$1
    sta   blockEntries

  fileGreaterThan:
    ; we've processed all entries, so just stop
    ProDOS_CLOSE fileio_close_param
    rts
 

END_SUB

.segment "RODATA_H"

DEFDATA error_file_msg
  .asciiz "Fatal ProDOS error: code = "
END_DEFDATA

.segment "DATA_H"

DEFDATA fileio_boot_unitnum
  .byte $00
END_DEFDATA

DEFDATA	fileio_boot_volname
  .res $40, $00
END_DEFDATA

DEFDATA fileio_open_param
  ProDOS_DEFINE_OPEN_PARAM fileio_buffer
END_DEFDATA

DEFDATA fileio_quit_param
  ProDOS_DEFINE_QUIT_PARAM 
END_DEFDATA

DEFDATA fileio_read_param
  ProDOS_DEFINE_READ_PARAM
END_DEFDATA

DEFDATA fileio_write_param
  ProDOS_DEFINE_WRITE_PARAM
END_DEFDATA

DEFDATA fileio_close_param
  ProDOS_DEFINE_CLOSE_PARAM
END_DEFDATA

DEFDATA fileio_create_param
  ProDOS_DEFINE_CREATE_PARAM
END_DEFDATA

DEFDATA fileio_on_line_param
  ProDOS_DEFINE_ON_LINE_PARAM
END_DEFDATA

DEFDATA fileio_getset_prefix_param
  ProDOS_DEFINE_GETSET_PREFIX_PARAM
END_DEFDATA

DEFDATA fileio_destroy_param
  ProDOS_DEFINE_DESTROY_PARAM
END_DEFDATA

.segment "DATA_L"

DEFDATA fileio_buffer 	; 1024 bytes
  .res $400, $00
END_DEFDATA
