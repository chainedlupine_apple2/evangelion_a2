
.segment "RODATA_H"

.include "../../build/nsr/assets/flyme1.inc"

.global IMG_MOON, _SLOW_MEMFILL

IMG_MOON:
.incbin "../../assets/nge-ending-moon.hgr"

IMG_ANIM:
.incbin "../../assets/nge-ending-rei.hgr"


.segment "CODE_H"



.macro SLOW_MEMFILL dstaddr, val, size
    Util_LOAD_AX_Addr dstaddr
    sta ROM::A1L
    stx ROM::A1H

    Util_LOAD_A_BVal val
    sta ROM::A2L

    Util_LOAD_AX_Addr size
    sta ROM::A3L
    stx ROM::A3H

    jsr _SLOW_MEMFILL
.endmacro
