# be careful, any BINFILE name must be 8 chars max
BUILDDST=build/nsr

CURR_PATH	:= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

BINFILE = ASMSTUB
BINFILE_PATH = $(BUILDDST)
BINFILE_FULL = $(BINFILE_PATH)/$(BINFILE).bin
OBJS = asmstub.o
LIBS = 
TARGET_ARCH = apple2
TARGET_MEMORY_MAP = src/nakedspinningrei/memory-asmstub.cfg

OBJ_PATH = $(BUILDDST)/obj

include lupinecore_a2/binasm.mk

CPU_TYPE = 6502

$(BUILDDST)/obj/asmstub.o: src/nakedspinningrei/asmstub-main.s
	$(CA65) $(AFLAGS) $< -o $@

.PHONY: all clean cleanassets assets dskHDD cleandsk dsk test cleantxt mount music cleanmusic test testimages copydsk

all: | bin dsk

clean: cleanbin
