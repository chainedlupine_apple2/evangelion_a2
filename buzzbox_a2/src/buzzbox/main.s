.segment "CODE_H"

.include "apple2rom.inc"
.include "prodos.inc"
.include "../zeropage.inc"
.include "utils.inc"

.include "buzzbox.inc"
.include "fileio.inc"

.export CHECK_FOR_BASIC, QUIT_TO_PRODOS, START_FROM_PRODOS

SUB MAIN
		; reset stack
		ldx     #$FF
		txs

		;JSR CHECK_FOR_BASIC

    lda #0
    sta ROM::ROM_WNDTOP
		lda #24
		sta ROM::ROM_WNDBTM
		lda #0
		sta ROM::ROM_WNDLFT
		lda #40
		sta ROM::ROM_WNDWDTH
    sta ROM::ROM_VTAB

		ZP_SETUP

		; initial ProDOS SYSTEM setup
    JSR START_FROM_PRODOS
		

		JSR ROM::TEXT_MODE_40
		JSR ROM::ROM_HOME ; home, clrscreen

		jsr FILE_GET_BOOT_UNIT_NUM_AND_VOLNAME

		jsr DO_TITLE

    jsr CLEAR_ALL_SONG_DATA
    jsr DUPLICATE_EXAMPLE_PATTERN

		jsr DRAW_UI_TOP

		
		JSR BUZZBOX_SONG_EDITOR
    

    JSR QUIT_TO_PRODOS
END_SUB

START_FROM_PRODOS:
		JSR ProDOS::RAMDISK_DISCONNECT
		RTS


SUB QUIT_TO_PRODOS
		lda #$00
		ldx #$30
		JSR ProDOS::RAMDISK_SET_FORMAT_BUFFER
		JSR ProDOS::RAMDISK_RECONNECT

		ProDOS_QUIT (quit_param)
END_SUB

CHECK_FOR_BASIC:
		lda $33
		cmp #$dd
		bne :++
			lda $76
			cmp #$ff
			bne :+
				JSR ROM::TEXT_MODE_40
				ROM_PRINT #txt_basic

				JSR ROM::ROM_GETKEY

				JMP QUIT_TO_PRODOS
			:
		:
		rts
		
; -- data ------------------------------------------------------

.segment "RODATA_H"

txt_basic:				.asciiz "Please boot straight from ProDOS        and do not use Applesoft. :)"

quit_param:
		ProDOS_DEFINE_QUIT_PARAM 
