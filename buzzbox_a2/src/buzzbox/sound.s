.segment "CODE_H"

.include "apple2rom.inc"
.include "../zeropage.inc"
.include "utils.inc"
.include "sound.inc"

.export PLAY_FREQ

; plays a note at a certain frequency
; freq is in freq, 1 = highest, 255 = lowest
; volume is 1-7
; duration is time, expressed as (roughly) seconds = (0.5*(5*(duration*duration)+27*duration+26))/10000
; note: if duration is too short, then lower frequencies won't play properly
PLAY_FREQ:
    lda sound_volume
    and #$7
    sta @loop1+1        ; adjust our volume suppression loops
    sta @sloop1+1
    tay
    lda compliment, y   ; do the same for the other balanced side of the loop
    sta @loop2+1
    sta @sloop2+1

    lda #0
    sta sound_freq_loop

@beep_start:
    ldx sound_duration
@beep_loop1:
    U_PHX
@beep_loop2:
    ; skip over beep if freq_pad_loop > 0
    lda sound_freq_loop
    beq :+
      jmp @silence
    :    
    ; speaker output (w/ 7 levels of volume)
    sta $c030         ; 4
    ;jmp @skip
@loop1:
    ldy #$01          ; 2
  :
    dey               ; 2
    bne :-            ; 2 or 3(+1)
    sta $c030         ; 4
@loop2:
    ldy #$07          ; 2
  :
    dey               ; 2
    bne :-        ; 2 or 3(+1)
    ; takes 17 cycles total
    jmp @incfreq

@silence:
    ; waste cycles in same exact amount as speaker routine
    sta extra         ; 4
@sloop1:
    ldy #$01          ; 2
  :
    dey               ; 2
    bne :-            ; 2 or 3(+1)
    sta extra         ; 4
@sloop2:
    ldy #$07          ; 2
  :
    dey               ; 2
    bne :-            ; 2 or 3(+1)
    ; takes 17 cycles total

@incfreq:
    ; increment frequency loop, if we hit it then reset to 0
    inc sound_freq_loop
    cmp sound_freq
    bcc :+
      lda #0
      sta sound_freq_loop
    :

@done:
    ; do first loop
    dex
    beq :+
      jmp @beep_loop2
    :
    ; do second loop
    U_PLX
    dex
    beq :+
      jmp @beep_loop1
    :

@exit:
    rts


.segment "DATA_H"

.export sound_duration, sound_freq, sound_freq_loop, sound_volume, extra

sound_duration:
  .byte 1
sound_freq:
  .byte 1
sound_freq_loop:
  .byte 1
sound_volume:   
  .byte 7

extra:
  .word $0000


.segment "RODATA_H"

.export compliment

compliment:
  .byte 8, 7, 6, 5, 4, 3, 2, 1

