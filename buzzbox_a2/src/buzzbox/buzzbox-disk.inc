.segment "CODE_H"
.include "prodos.inc"
.include "fileio.inc"

SUB DO_DISK
    inkey = temp1

    jsr REDRAW_DISK_UI

		sta ROM::SB_KEYB

  loop:
	:	lda ROM::IN_KEYB
		bpl :-

    and #$7F
    jsr CONV_TO_UPPER
    sta inkey

		sta ROM::SB_KEYB

    lda inkey
    cmp #ROM::KB::kb_escape
    bne :+
      rts
    :

    cmp #ROM::KB::kb_R
    bne :+
      jsr HANDLE_RESCAN
    :

    cmp #ROM::KB::kb_C
    bne :+
      jsr HANDLE_CATALOG
    :

    cmp #ROM::KB::kb_S
    bne :+
      jsr HANDLE_SET_ACTIVE_DRIVE
    :

    cmp #ROM::KB::kb_P
    bne :+
      jsr HANDLE_ENTER_PREFIX
    :

    cmp #ROM::KB::kb_D
    bne :+
      jsr HANDLE_DELETE_FILE
    :

    jmp loop
END_SUB

SUB HANDLE_DELETE_FILE
    jsr CLEAR_DISK_SCROLL_WINDOW
    jsr RESET_DISK_SCROLL_WINDOW

    lda #0
    ldx #8
    jsr SET_CURSOR

    ROM_PRINT #txt_disk_delete_name

    jsr UI_ENTER_PREFIX
    bcc :+
      jmp no
    :

    lda #0
    ldx #10
    jsr SET_CURSOR

    ROM_PRINT_CINV #txt_disk_delete_confirm

    jsr ASK_YES_NO
    bcc no
    Util_LOAD #textinput_entered, arg1w
    jsr FILE_DESTROY
    bcc :+
      pha
      jsr PLAY_ERROR
      pla
      jsr DISPLAY_PRODOS_IO_ERROR
      jmp no
    :
    jsr PLAY_LONG
  no:
    jsr CLEAR_DISK_SCROLL_WINDOW
    jsr RESET_DISK_SCROLL_WINDOW

    rts
END_SUB

SUB HANDLE_ENTER_PREFIX
    jsr CLEAR_DISK_SCROLL_WINDOW
    jsr RESET_DISK_SCROLL_WINDOW

    lda #0
    ldx #8
    jsr SET_CURSOR

    ROM_PRINT #txt_disk_get_prefix

    lda #0
    ldx #9
    jsr SET_CURSOR

    jsr UI_ENTER_PREFIX
    bcs abort
    ; now set the prefix
    Util_LOAD #textinput_entered, arg1w
    jsr FILE_SET_PREFIX
    bcs error

    Util_LOAD #textinput_entered, arg1w
    jsr FILE_LSTR_TO_BOOTVOLNAME

    ; now find out what unitnum this path corresponds to
    jsr FILE_GET_BOOT_UNIT_NUM
    bcs error

    jsr DRAW_DISK_UI_EXTRA
    jmp abort

  error:
    pha
    jsr PLAY_ERROR
    pla
    jsr DISPLAY_PRODOS_IO_ERROR

  abort:
    jsr CLEAR_DISK_SCROLL_WINDOW
    jsr RESET_DISK_SCROLL_WINDOW
    rts
END_SUB

SUB HANDLE_SET_ACTIVE_DRIVE
    slot        = temp1
    drive       = temp2
    oldbootnum  = temp3

    lda fileio_boot_unitnum
    sta oldbootnum
reset:
    jsr CLEAR_DISK_SCROLL_WINDOW
    jsr RESET_DISK_SCROLL_WINDOW

    lda #0
    ldx #8
    jsr SET_CURSOR

    ROM_PRINT #txt_disk_set_active_slot

    jsr UI_ENTER_NUMBER_SINGLE
    bcs abort
      
    lda textinput_number
    beq error
    cmp #8
    bcs error
    sta slot

    ; now enter drive number
    ROM_PRINT #txt_disk_set_active_drive

    jsr UI_ENTER_NUMBER_SINGLE
    bcs abort
      
    lda textinput_number
    beq error
    cmp #3
    bcs error
    sta drive
    dec drive ; to turn to zero base

    ; we got slot/drive
    lda slot
    asl
    asl
    asl
    asl
    sta fileio_boot_unitnum
    lda drive
    beq :+
      lda fileio_boot_unitnum
      ora #$80
      sta fileio_boot_unitnum
  :

    jsr FILE_RESYNC_VOLUME_NAME
    bcs :+
      jsr FILE_TEMPVOLNAME_TO_BOOTVOLNAME
      jsr DRAW_DISK_UI_EXTRA

      jsr CLEAR_DISK_SCROLL_WINDOW
      jsr RESET_DISK_SCROLL_WINDOW
      rts
    :
    jsr DISPLAY_PRODOS_IO_ERROR
    lda oldbootnum
    sta fileio_boot_unitnum
    jsr REDRAW_DISK_UI
    jmp exit

  error:
    jsr PLAY_ERROR
    jmp reset
  
  abort:
    jsr CLEAR_DISK_SCROLL_WINDOW
    jsr RESET_DISK_SCROLL_WINDOW

  exit:
    rts
END_SUB



max_scroll_lines = 24 - 8 - 1

; arg1w = directory entry
SUB HANDLE_CATALOG_CALLBACK
    entryPtr = arg1w
    fileType = temp1
    ZP_SAVE
    ldy #0
    lda (entryPtr), y
    and #$f0
    lsr
    lsr
    lsr
    lsr
    sta fileType
    cmp #$4
    beq @invalidfile
      ; we have a valid file
      ldy #0
      lda (entryPtr), y
      and #$0f

      sta disk_temp_filename

      U_INC
      tax
      tay
    @cloop:
      lda (entryPtr), y
      sta disk_temp_filename, x
      dey
      dex
      bne @cloop

      ; print name
      ROM_LSTR_PRINT #disk_temp_filename

      lda fileType
      cmp #$0D
      bne :+
        ROM_PRINT_CINV #txt_dir_prefix
      :

      jsr ROM::ROM_CROUT

      inc disk_scroll_line
      lda disk_scroll_line
      cmp #max_scroll_lines
      bcc :+
        lda #0
        sta disk_scroll_line
        jsr ROM::ROM_GETKEY
      :
  @invalidfile:
    ; skip entry, because it's a directory or pascal area

    ZP_RESTORE
    rts
END_SUB

SUB CLEAR_DISK_SCROLL_WINDOW
    lda #8
    sta ROM::ROM_WNDTOP
    sta ROM::ROM_VTAB

    lda #0
    ldx #8
    jsr SET_CURSOR

    jsr ROM::ROM_CLREOP

    rts
END_SUB

SUB RESET_DISK_SCROLL_WINDOW
    lda #0
    sta ROM::ROM_WNDTOP
    sta ROM::ROM_VTAB
    rts
END_SUB

SUB HANDLE_CATALOG
    ;JSR ROM::ROM_HOME

    ;ROM_LSTR_PRINT #fileio_boot_volname
    ;jsr ROM::ROM_CROUT
    ;jsr ROM::ROM_CROUT

    jsr CLEAR_DISK_SCROLL_WINDOW

    lda #0
    sta disk_scroll_line

    Util_LOAD #dir_buffer, arg1w
    Util_LOAD #HANDLE_CATALOG_CALLBACK, arg2w
    jsr FILE_SHOW_CATALOG
    bcs :+
      ;jsr ROM::ROM_GETKEY
      jsr RESET_DISK_SCROLL_WINDOW

      rts
    :
    jsr DISPLAY_PRODOS_IO_ERROR
    jsr REDRAW_DISK_UI
    rts
END_SUB

SUB HANDLE_RESCAN
    jsr FILE_RESYNC_VOLUME_NAME
    bcs :+
      jsr FILE_TEMPVOLNAME_TO_BOOTVOLNAME
      jsr DRAW_DISK_UI_EXTRA

      jsr CLEAR_DISK_SCROLL_WINDOW
      jsr RESET_DISK_SCROLL_WINDOW
      rts
    :
    jsr DISPLAY_PRODOS_IO_ERROR
    jsr FILE_TEMPVOLNAME_TO_BOOTVOLNAME
    jsr REDRAW_DISK_UI
    rts
END_SUB

SUB REDRAW_DISK_UI
    JSR ROM::ROM_HOME ; home, clrscreen

    jsr DRAW_DISK_UI

    jsr DRAW_DISK_UI_EXTRA

END_SUB

SUB DRAW_DISK_UI_EXTRA    
    lda #0
    ldx #6
    jsr SET_CURSOR

    jsr ROM::ROM_CLREOL

    ROM_PRINT #txt_disk_curr_prefix

    ROM_LSTR_PRINT #fileio_boot_volname

    lda #30
    ldx #2
    jsr SET_CURSOR

    jsr ROM::ROM_CLREOL

    lda #'S' + $80
    jsr ROM::ROM_COUT

    lda fileio_boot_unitnum
    and #$80
    beq :+
      ; drive #1
      lda fileio_boot_unitnum
      and #$7F
      lsr
      lsr
      lsr
      lsr
      jsr ROM::ROM_HEX
      lda #',' + $80
      jsr ROM::ROM_COUT

      lda #'1' + $80
      jsr ROM::ROM_COUT
      rts
    :
    ; drive #0
    lda fileio_boot_unitnum
    lsr
    lsr
    lsr
    lsr
    jsr ROM::ROM_HEX

    lda #',' + $80
    jsr ROM::ROM_COUT

    lda #'0' + $80
    jsr ROM::ROM_COUT

    rts
END_SUB

SUB DRAW_DISK_UI
    lda #0
    ldx #0
    jsr SET_CURSOR

    ROM_PRINT_CINV #txt_disk_title

    lda #2
    ldx #2
    jsr SET_CURSOR

    ROM_PRINT_CINV_LINES #txt_disk_help

    jsr DRAW_DISK_UI_EXTRA

    rts
END_SUB

SUB CLEAR_DISK_UI
    ldy #7
  loop:
    tya
    clc
    adc #3
    tax
    lda #1
    U_PHY
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL
    U_PLY
    dey
    bne loop
    rts
END_SUB

SUB COPY_SONGNAME_TO_TEXTINPUT
    ldx #0
  loop:
    lda songname, x
    beq done 
    sta textinput_buffer, x
    inx
    cpx #8
    bcc loop
  done:
    stx textinput_entered
    rts
END_SUB

; return: carry if error/aborted
SUB GET_FILENAME
    ; copy curren song name into textinput_buffer
    jsr COPY_SONGNAME_TO_TEXTINPUT
    jsr UI_ENTER_STRING_PREFILLED
    rts
END_SUB

; uses textinput_buffer
SUB ADD_FILE_EXT
    ldx #0
  addloop:
    lda txt_ext, x
    sta temp1
    txa
    clc
    adc textinput_entered
    U_PHX
    tax
    lda temp1
    sta textinput_buffer, x
    U_PLX
    inx
    cpx #4
    bcc addloop

    inc textinput_entered
    inc textinput_entered
    inc textinput_entered
    inc textinput_entered

    rts
END_SUB

song_data_length := song_data_end_marker - song_data_begin_marker

SUB SAVE_SONG
    ; filename should be in textinput_buffer (start with textinput_entered to get a full LSTR)
    jsr ADD_FILE_EXT
    Util_LOAD #textinput_entered, arg1w
    jsr FILE_CREATE
    ; first we need to create the file
    bcc continue
      ; carry set, so there was an error.  Let's examine it
      cmp #ProDOS::PRODOS_ERR_DUPLICATE_FILE
      bne :+
        ; whatever, just continue
        jmp continue
      :
      ; if we are not a duplicate file, then it's a real error and we need to tell user
      jsr DISPLAY_PRODOS_IO_ERROR
      jmp done

    ; file has been created
  continue:
    jsr FILE_OPEN
    bcc file_open ; failure to open file
    jsr DISPLAY_PRODOS_IO_ERROR
    jmp done

    ; file is open, let's do our business
  file_open:
    jsr FILE_PREPARE

    jsr CALCULATE_SONG_HEADER

    Util_LOAD #songheader, arg1w
    Util_LOAD #song_data_length, arg2w

    jsr FILE_WRITE
    bcc :+
      php
      pha
      jsr FILE_CLOSE
      pla
      plp

      jsr DISPLAY_PRODOS_IO_ERROR
      jmp done
    :

    jsr FILE_CLOSE
    bcc done

    ; error on close
    jsr DISPLAY_PRODOS_IO_ERROR
    jmp done
  done:
    rts
END_SUB

SUB LOAD_SONG
    ; filename should be in textinput_buffer (start with textinput_entered to get a full LSTR)
    jsr ADD_FILE_EXT
    Util_LOAD #textinput_entered, arg1w
    jsr FILE_OPEN
    bcc file_open ; failure to open file
    jsr DISPLAY_PRODOS_IO_ERROR
    jmp done

    ; file is open, let's do our business
  file_open:
    jsr FILE_PREPARE

    Util_LOAD #songheader, arg1w
    Util_LOAD #song_data_length, arg2w

    jsr FILE_READ
    bcc file_read

    php
    pha
    jsr FILE_CLOSE
    pla
    plp

    ; error on read
    jsr DISPLAY_PRODOS_IO_ERROR
    jmp done

  file_read:
    jsr FILE_CLOSE
    bcc :+
      ; error on close
      jsr DISPLAY_PRODOS_IO_ERROR
      jmp done
    :

    ; alright, let's verify the header
    jsr VERIFY_SONG_HEADER
    bcc done

    jsr CLEAR_UI_PATTERN_KEYS

    lda #0
    ldx #17
    jsr SET_CURSOR

    ROM_PRINT #txt_file_load_error

    jsr CLEAR_ALL_SONG_DATA

    jsr ROM::ROM_GETKEY

  done:
    rts
END_SUB


SUB DISPLAY_PRODOS_IO_ERROR
    pha
    jsr CLEAR_UI_PATTERN_KEYS

    lda #0
    ldx #17
    jsr SET_CURSOR

    ROM_PRINT #txt_disk_error

    lda #0
    ldx #19
    jsr SET_CURSOR

    Util_LOAD #table_prodos_errors, temp1w

    pla
    sta temp1

    ldy #0
  loop:
    lda (temp1w), y
    beq unknown
    cmp temp1
    bne :+
      iny
      lda (temp1w), y
      sta temp2w
      iny
      lda (temp1w), y
      sta temp2w+1
      ROM_PRINT temp2w
      jmp done
    :
    ; keep searching
    iny
    iny
    iny
    jmp loop

  unknown:
    ROM_PRINT #txt_pderr_unknown
    lda temp1
    jsr ROM::ROM_PRBYTE
  done:
    jsr ROM::ROM_GETKEY
    rts

END_SUB

.segment "DATA_H"

DEFDATA disk_temp_filename  ; 15 chars+1
  .res $10
END_DEFDATA



.segment "RODATA_H"

DEFDATA table_prodos_errors
  .byte ProDOS::PRODOS_ERR_IO
  .addr txt_pderr_IO
  
  .byte ProDOS::PRODOS_ERR_WRITE_PROTECT
  .addr txt_pderr_WRITE_PROTECT

  .byte ProDOS::PRODOS_ERR_BAD_PATHNAME
  .addr txt_pderr_BAD_PATHNAME
  
  .byte ProDOS::PRODOS_ERR_VOLUME_NOT_FOUND
  .addr txt_pderr_VOLUME_NOT_FOUND
  
  .byte ProDOS::PRODOS_ERR_FILE_NOT_FOUND
  .addr txt_pderr_FILE_NOT_FOUND
  
  .byte ProDOS::PRODOS_ERR_NOT_PRODOS
  .addr txt_pderr_NOT_PRODOS
  
  .byte ProDOS::PRODOS_ERR_DUPLICATE_FILE
  .addr txt_pderr_DUPLICATE_FILENAME
  
  .byte ProDOS::PRODOS_ERR_OVERRUN
  .addr txt_pderr_OVERRUN
  
  .byte ProDOS::PRODOS_ERR_VOLUME_DIR_FULL
  .addr txt_pderr_VOL_DIR_FULL

  .byte ProDOS::PRODOS_ERR_EOF
  .addr txt_pderr_FILE_SIZE_TOO_SMALL

  .byte ProDOS::PRODOS_ERR_NO_DEVICE
  .addr txt_pderr_DEVICE_NOT_FOUND

  .byte 0
END_DEFDATA

DEFDATA txt_dir_prefix
  INVBYTE '/'
  .byte 0
END_DEFDATA

DEFDATA txt_ext
  .byte ".BBT"
END_DEFDATA

DEFDATA disk_scroll_line
  .byte $00
END_DEFDATA

DEFDATA txt_file_load_error
  .asciiz "MUSIC FILE IS CORRUPT"
END_DEFDATA

DEFDATA txt_pderr_FILE_SIZE_TOO_SMALL
  .asciiz "FILE TOO SMALL"
END_DEFDATA

DEFDATA txt_pderr_unknown
  .asciiz "UNKNOWN PRODOS ERR $"
END_DEFDATA

DEFDATA txt_pderr_IO
  .asciiz "I/O ERROR"
END_DEFDATA

DEFDATA txt_pderr_WRITE_PROTECT
  .asciiz "WRITE PROTECTED"
END_DEFDATA

DEFDATA txt_pderr_BAD_PATHNAME
  .asciiz "BAD PATHNAME"
END_DEFDATA

DEFDATA txt_pderr_VOLUME_NOT_FOUND
  .asciiz "CANNOT FIND VOLUME"
END_DEFDATA

DEFDATA txt_pderr_FILE_NOT_FOUND
  .asciiz "CANNOT FIND FILE"
END_DEFDATA

DEFDATA txt_pderr_NOT_PRODOS
  .asciiz "NOT A PRODOS DISK"
END_DEFDATA

DEFDATA txt_pderr_DUPLICATE_FILENAME
  .asciiz "DUP FILENAME"
END_DEFDATA

DEFDATA txt_pderr_OVERRUN
  .asciiz "OUT OF SPACE ON DISK"
END_DEFDATA

DEFDATA txt_pderr_VOL_DIR_FULL
  .asciiz "VOLUME DIR FULL"
END_DEFDATA

DEFDATA txt_pderr_DEVICE_NOT_FOUND
  .asciiz "DEVICE NOT CONNECTED"
END_DEFDATA

DEFDATA txt_disk_curr_prefix
  .asciiz "VOLUME PREFIX: "
END_DEFDATA

DEFDATA txt_disk_title
    .byte "BBT Disk/Path Manipulation ("
    INVBYTE 'E'
    INVBYTE 'S'
    INVBYTE 'C'
    .byte "=back)", 0
END_DEFDATA

DEFDATA txt_disk_error
  .asciiz "ERROR ON DISK ACCESS:"
END_DEFDATA

DEFDATA txt_disk_set_active_slot
  .asciiz "SLOT (1-7)? "
END_DEFDATA

DEFDATA txt_disk_set_active_drive
  .asciiz "  DRIVE (1-2)? "
END_DEFDATA

DEFDATA txt_disk_get_prefix
  .asciiz "ENTER PREFIX:  (esc=stop, no / at end)"
END_DEFDATA

DEFDATA txt_disk_delete_name
  .asciiz "FILENAME TO DELETE? "
END_DEFDATA

DEFDATA txt_disk_delete_confirm
  .byte "CONFIRM DELETE "
  INVBYTE 'Y'
  .byte "/"
  INVBYTE 'N'
  .byte "? "
  .byte 0
END_DEFDATA

DEFDATA txt_disk_help
  INVBYTE 'R'
  .byte "ESCAN/"
  INVBYTE 'C'
  .byte "ATALOG ACTIVE DRIVE", $0D
  .byte "SET "
  INVBYTE 'P'
  .byte "REFIX MANUALLY - "
  INVBYTE 'D'
  .byte "ELETE FILE", $0D
  INVBYTE 'S'
  .byte "ET ACTIVE SLOT,DISK", $0D
  .byte 0
END_DEFDATA

.segment "DATA_H"

