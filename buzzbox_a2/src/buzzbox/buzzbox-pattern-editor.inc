.segment "CODE_H"

.export HANDLE_PATTERN_KEYBOARD, HANDLE_EDIT, HANDLE_KB_LEFT, HANDLE_KB_PLAY, HANDLE_KB_RIGHT, HANDLE_NULL, HANDLE_PAGE_LEFT, HANDLE_PAGE_RIGHT, HANDLE_PRACTICE
.export DUPLICATE_EXAMPLE_PATTERN, CLEAR_ALL_PATTERNS, MUSIC_KEYBOARD_ABORT_CALLBACK, PLAY_PATTERN_WITH_CALLBACK, PLAY_CLICK, PLAY_THUNK
.export BUZZBOX_PATTERN_EDITOR

song_len = 128
max_patterns = 32

SUB BUZZBOX_PATTERN_EDITOR
    jsr ROM::ROM_HOME

    jsr SET_PATTERN_PTR

    jsr DRAW_UI_TOP

    jsr DRAW_UI_PATTERN_TOP

    lda #3
    sta sound_volume

    lda #20
    ldx #10
    JSR SET_CURSOR

    lda #0
    ldx #10
    JSR SET_CURSOR

    JSR ROM::ROM_CLREOL

    jsr CALC_PATTERN_CURSOR

    JSR DRAW_UI_PATTERN

    JSR DRAW_PATTERN

    sec
    JSR DRAW_PATTERN_CURSOR

  loop:
    jsr HANDLE_PATTERN_KEYBOARD    
    bcc loop

    rts

END_SUB

SUB HANDLE_PATTERN_KEYBOARD
    inkey = temp1

	:	lda ROM::IN_KEYB
		bpl :-
		sta ROM::SB_KEYB
		and #$7f
    jsr CONV_TO_UPPER
		sta inkey

		; key press in temp1
		lda inkey
    cmp #ROM::KB::kb_escape
    bne :+
      sec
      rts
  :

    cmp #ROM::KB::kb_left_arrow
		bne :+
		jmp HANDLE_KB_LEFT
	:
    cmp #ROM::KB::kb_right_arrow
		bne :+
		jmp HANDLE_KB_RIGHT
	:
    cmp #ROM::KB::kb_P
    bne :+
    jmp HANDLE_KB_PLAY
  :

    cmp #ROM::KB::kb_LEFTBRACKET
    bne :+
    jmp HANDLE_PAGE_LEFT
  :

    cmp #ROM::KB::kb_RIGHTBRACKET
    bne :+
    jmp HANDLE_PAGE_RIGHT
  :

    cmp #ROM::KB::kb_N
    bne :+
    jmp HANDLE_NULL
  :

    cmp #ROM::KB::kb_M
    bne :+
    jmp HANDLE_RENAME
  :

    cmp #ROM::KB::kb_E
    bne :+
    jmp HANDLE_EDIT
  :

    cmp #ROM::KB::kb_K
    bne :+
    jmp HANDLE_DELETE
  :

    cmp #ROM::KB::kb_T
    bne :+
    jmp HANDLE_SETTINGS
  :

    cmp #ROM::KB::kb_D
    bne :+
    jmp HANDLE_DISK_FROM_PATTERN
  :

    cmp #ROM::KB::kb_C
    bne :+
    jmp HANDLE_CLEAR
  :

    cmp #ROM::KB::kb_I
    bne :+
    jmp HANDLE_INSERT_PIANO
  :

    cmp #ROM::KB::kb_A
    bne :+
    jmp HANDLE_INSERT_RAWMODE
  :

    cmp #ROM::KB::kb_R
    bne :+
    jmp HANDLE_PRACTICE
  :

    cmp #ROM::KB::kb_S
    bne :+
    jmp HANDLE_KB_SAVE
  :

    cmp #ROM::KB::kb_L
    bne :+
    jmp HANDLE_KB_LOAD
  :

  jmp PATTERN_HANDLER_EXIT
END_SUB
    ; draw unknown key for debugging
    ;lda #20
    ;ldx #15
    ;jsr SET_CURSOR
    ;lda temp1
    ;jsr ROM::ROM_PRBYTE

PATTERN_HANDLER_EXIT:
    clc
    rts

SUB HANDLE_RENAME
    pattnameoffset = temp1
    jsr CLEAR_UI_PATTERN_KEYS

    lda #0
    ldx #19
    jsr SET_CURSOR
    ROM_PRINT #txt_pattern_rename_label

    jsr UI_ENTER_STRING
    bcs :+
      ; have string name, now replace
      lda currpattern
      asl
      asl
      asl ; 8x
      sta pattnameoffset

      ldx #0
    loop:
      lda textinput_buffer, x
      pha      
      txa
      tay
      clc
      adc pattnameoffset
      tax
      pla
      sta patternnames, x
      tya
      tax
      inx
      cpx #8
      bcc loop

      jsr RESTORE_PATTERN_UI
      jmp PATTERN_HANDLER_EXIT
    :

    jsr DRAW_UI_PATTERN_KEYS    

    jmp PATTERN_HANDLER_EXIT
END_DEFDATA

HANDLE_SETTINGS:
    jsr DO_SETTINGS_EDITOR

    jsr RESTORE_PATTERN_UI

    jmp PATTERN_HANDLER_EXIT

HANDLE_DISK_FROM_PATTERN:
    jsr DO_DISK

    jsr RESTORE_PATTERN_UI

    jmp PATTERN_HANDLER_EXIT

SUB HANDLE_INSERT_PIANO
    jsr INSERT_PIANO_MODE
    jmp PATTERN_HANDLER_EXIT
END_SUB

SUB HANDLE_INSERT_RAWMODE
    jsr INSERT_RAW_MODE
    jmp PATTERN_HANDLER_EXIT
END_SUB

HANDLE_PRACTICE:
    lda #5
    sta ui_piano_volume
    lda #du_3
    sta ui_piano_duration
    lda #2
    sta ui_piano_octave

    lda #0
    sta ui_piano_insert_mode

    jsr STARTUP_PIANO

  @loop:
    jsr DO_PIANO
    bcc :+
      jmp @exit
    :
    ; do nothing with the result
    jmp @loop

  @exit:
    jsr SHUTDOWN_PIANO
    jmp PATTERN_HANDLER_EXIT

SUB HANDLE_EDIT
    jsr CLEAR_UI_PATTERN_KEYS

    lda #0
    ldx #17
    jsr SET_CURSOR
    ROM_PRINT #txt_pattern_edit1b

    lda #0
    ldx #19
    jsr SET_CURSOR
    ROM_PRINT #txt_pattern_edit1a

    Util_LOAD currpatternptr, temp1w
    lda pattern_cursor
    asl ; 2x
    tay
    lda (temp1w),y
    sta temp3
    iny
    lda (temp1w),y
    sta temp4

    jsr UI_ENTER_NUMBER
    ; carry on error (or abort)
    bcs :+
      jsr PLAY_THUNK
      ; now our number should be in text_number
      lda textinput_number
      sta temp3
      jmp note_entered
    :
    beq abort ; if a=0, then escape pressed so let's just abort

  note_entered:
    jsr CLEAR_UI_PATTERN_KEYS

    lda #0
    ldx #17
    jsr SET_CURSOR
    ROM_PRINT #txt_pattern_edit2b

    lda #0
    ldx #19
    jsr SET_CURSOR
    ROM_PRINT #txt_pattern_edit2a

    jsr UI_ENTER_NUMBER
    ; carry on error (or abort)
    bcs :+
      jsr PLAY_THUNK

      ; now our number should be in text_number
      lda textinput_number
      sta temp4
      jmp dur_entered
    :
    beq abort ; if a=0, then escape so abort    

  dur_entered:
    jsr PLAY_CLICK
    jsr PLAY_CLICK

    ; write new data
    Util_LOAD currpatternptr, temp1w
    lda pattern_cursor
    asl ; 2x
    tay
    lda temp3
    sta (temp1w),y
    iny
    lda temp4
    sta (temp1w),y

  abort:
    jsr DRAW_UI_PATTERN_KEYS
    jsr DRAW_PATTERN

    jmp PATTERN_HANDLER_EXIT
END_SUB

.export HANDLE_DELETE

HANDLE_DELETE:

    lda pattern_cursor
    jsr DELETE_PATTERN_NOTE

    jsr DRAW_PATTERN
    jmp PATTERN_HANDLER_EXIT

HANDLE_CLEAR:
    jsr CLEAR_UI_PATTERN_KEYS

    lda #0
    ldx #17
    jsr SET_CURSOR
    ROM_PRINT_CINV #txt_pattern_clearpattern

    jsr ASK_YES_NO
    bcc :+
      jsr CLEAR_PATTERN_CURSOR

      jsr CLEAR_CURRENT_PATTERN

      lda #0
      sta pattern_cursor

      jsr DRAW_PATTERN

      jsr CALC_PATTERN_CURSOR
      jsr DRAW_PATTERN_CURSOR
      jsr DRAW_PATTERN_EXTRA

      jsr PLAY_LONG
    :

    jsr DRAW_UI_PATTERN_KEYS
    jmp PATTERN_HANDLER_EXIT

HANDLE_NULL:
    jsr CLEAR_UI_PATTERN_KEYS

    lda #0
    ldx #17
    jsr SET_CURSOR

    ROM_PRINT #txt_pattern_null

    jsr UI_ENTER_NUMBER
    ; carry on error (or abort)
    bcs @abort
      ;lda textinput_number
      ;jsr ROM::ROM_PRBYTE
      ;ROM_PRINT #textinput_buffer
      ;jsr GETKEYJSR

      jsr PLAY_THUNK
      ; now our number should be in text_number
      ; limit size
      lda textinput_number
      cmp #$80
      bcc :+
      lda #$80
      sta textinput_number
    :

      ; null out these indexes
      Util_LOAD currpatternptr, temp1w

      ldx textinput_number
      lda pattern_cursor
      asl ; 2x
      tay
    :
      lda #0
      sta (temp1w),y
      iny
      sta (temp1w),y
      iny
      dex
      bne :-


  @abort:
    jsr DRAW_UI_PATTERN_KEYS
    jsr DRAW_PATTERN

    jmp PATTERN_HANDLER_EXIT

HANDLE_PAGE_LEFT:
    jsr CLEAR_PATTERN_CURSOR

    lda pattern_cursor
    sec
    sbc #13
    sta pattern_cursor
    cmp #$7F
    bcc :+
      lda #$7F
      sta pattern_cursor
    :

    jsr CALC_PATTERN_CURSOR
    jsr DRAW_PATTERN_CURSOR
    jsr DRAW_PATTERN_EXTRA
    
    jmp PATTERN_HANDLER_EXIT

HANDLE_PAGE_RIGHT:
    jsr CLEAR_PATTERN_CURSOR

    lda pattern_cursor
    clc
    adc #13
    sta pattern_cursor
    cmp #$7F
    bcc :+
      lda #$0
      sta pattern_cursor
    :

    jsr CALC_PATTERN_CURSOR
    jsr DRAW_PATTERN_CURSOR
    jsr DRAW_PATTERN_EXTRA

    jmp PATTERN_HANDLER_EXIT

HANDLE_KB_PLAY:
    jsr PLAY_ALL_PATTERN

    jmp PATTERN_HANDLER_EXIT

HANDLE_KB_LEFT:
    jsr MOVE_PATTERN_CURSOR_LEFT

    jmp PATTERN_HANDLER_EXIT

HANDLE_KB_RIGHT:    
    jsr MOVE_PATTERN_CURSOR_RIGHT

    jmp PATTERN_HANDLER_EXIT

HANDLE_KB_SAVE:
    jsr CLEAR_UI_PATTERN_KEYS

    lda #0
    ldx #17
    jsr SET_CURSOR

    ROM_PRINT #txt_save_filename

    jsr GET_FILENAME
    bcs :+
      jsr SAVE_SONG    
    :

    jsr RESTORE_PATTERN_UI
    
    jmp PATTERN_HANDLER_EXIT

HANDLE_KB_LOAD:
    jsr CLEAR_UI_PATTERN_KEYS

    lda #0
    ldx #17
    jsr SET_CURSOR

    ROM_PRINT #txt_load_filename

    jsr GET_FILENAME
    bcs :+
      jsr LOAD_SONG
    :

    jsr RESTORE_PATTERN_UI
    
    jmp PATTERN_HANDLER_EXIT


RESTORE_PATTERN_UI:
    jsr ROM::ROM_HOME

    jsr DRAW_UI_TOP
    jsr DRAW_UI_PATTERN_TOP
    jsr DRAW_PATTERN
    jsr DRAW_PATTERN_EXTRA
    jsr DRAW_PATTERN_CURSOR
    jsr DRAW_UI_PATTERN
    
    jsr DRAW_UI_PATTERN_KEYS

    rts


.segment "RODATA_H"

.export txt_pattern_null, txt_pattern_edit1a, txt_pattern_edit1b, txt_pattern_edit2a, txt_pattern_edit2b, txt_ui_pattern_len, txt_ui_pattern_cmds1, txt_ui_pattern_cmds2, txt_ui_pattern_cmds3

DEFDATA txt_save_filename
  .asciiz "SAVE FILE? (ESC=abort): "
END_DEFDATA

DEFDATA txt_load_filename
  .asciiz "LOAD FILE? (ESC=abort): "
END_DEFDATA

txt_pattern_null:
  .asciiz "HOW MANY TO NULL (ESC=stop): "

txt_pattern_edit1a:
  .asciiz "ENTER FREQ/COMMAND: "
txt_pattern_edit1b:
  .asciiz "<F0=NOTE, >F0=CMD, ENTER=skip, ESC=stop"

txt_pattern_edit2a:
  .asciiz "DURATION/DATA: "
txt_pattern_edit2b:
  .asciiz ">80=HALF VOL, ENTER=skip, ESC=stop"

DEFDATA txt_pattern_clearpattern
  .byte "CLEAR WHOLE PATTERN? "
  INVBYTE 'Y'
  .byte "/"
  INVBYTE 'N'
  .byte " "
  .byte 0
END_DEFDATA

DEFDATA txt_ui_pattern_top
  .asciiz "PATT "
END_DEFDATA

DEFDATA txt_ui_pattern_top_end
  .asciiz "/1F"
END_DEFDATA

DEFDATA txt_pattern_rename_label
  .asciiz "NEW PATTERN NAME? (ESC=abort) "
END_DEFDATA

txt_ui_pattern_cmds1:
  INVBYTE 'E'
  .byte "DIT P"
  INVBYTE 'I'
  .byte "ANO/R"
  INVBYTE 'A'
  .byte "W_INSERT "
  INVBYTE 'K'
  .byte "ILL_NOTE "

  .byte 0

txt_ui_pattern_cmds2:
  INVBYTE 'P'
  .byte "LAY "
  INVBYTE 'C'
  .byte "LEAR "
  INVBYTE 'N'
  .byte "ULL P"
  INVBYTE 'R'
  .byte "ACTICE "
  .byte "RENA"
  INVBYTE 'M'
  .byte "E "

  .byte 0

txt_ui_pattern_cmds3:
  INVBYTE 'S'
  .byte "AVE "
  INVBYTE 'L'
  .byte "OAD "
  INVBYTE 'D'
  .byte "ISK "
  INVBYTE 'E'
  INVBYTE 'S'
  INVBYTE 'C'
  .byte "=back"
  .byte 0

.segment "DATA_H"

.export patternstorage, currpatternptr, pattern_cursor, pattern_cursorx, pattern_offset, examplesong, temp, song_data_end_marker, patternnames, backup_pattern, currpatternnameptr, songname, seqstorage, seqptrstorage, dir_buffer


seqptrstorage:
  .res song_len * 2


; used as undo buffer (and as also temp buffer for reading DIR file)
backup_pattern:
dir_buffer:
  .res 4 * song_len

currpatternptr:
  .word $0000

currpatternnameptr:
  .word $0000

currpattern:
  .byte 0

pattern_offset:
  .byte 0

pattern_cursor:
  .byte 0

pattern_cursorx:
  .byte 0


.segment "RODATA_H"

DEFDATA txt_default_pattern_name
  .asciiz "NULL"
END_DEFDATA

DEFDATA txt_default_song_name
  .asciiz "SONG"
END_DEFDATA

; songs are series of two-byte commands
; first byte           second byte
; <128 = note          duration  (if >128, then halve note volume)
; >=128 = command      <command specific>
examplesong:
  .byte cmd_v,    3

  .byte no_4_C,   du_4
  .byte no_4_Cs,  du_4
  .byte no_4_D,   du_4
  .byte no_4_Ds,  du_4
  .byte no_4_E,   du_4
  .byte no_4_F,   du_4
  .byte no_4_Fs,  du_4 + no_hv
  .byte no_4_G,   du_4 + no_hv
  .byte no_4_Gs,  du_4 + no_hv
  .byte no_4_A,   du_4 + no_hv
  .byte no_4_As,  du_4 + no_hv
  .byte no_4_B,   du_4 + no_hv

  .byte cmd_d,    du_9
  
  .byte cmd_v,    1

  .byte no_4_B,   du_2
  .byte no_4_As,  du_2
  .byte cmd_v,    2
  .byte no_4_A,   du_2
  .byte no_4_Gs,  du_2
  .byte cmd_v,    3
  .byte no_4_G,   du_2
  .byte no_4_Fs,  du_2
  .byte cmd_v,    4
  .byte no_4_F,   du_2
  .byte no_4_E,   du_2
  .byte cmd_v,    5
  .byte no_4_Ds,  du_2
  .byte no_4_D,   du_2
  .byte cmd_v,    6
  .byte no_4_Cs,  du_2
  .byte no_4_C,   du_2

  .byte cmd_d,    du_9
  .byte cmd_d,    du_9

  .byte cmd_v,    7

  .byte $20,      du_1
  .byte cmd_d,    du_3
  .byte $20,      du_1
  .byte cmd_d,    du_3
  .byte $20,      du_1
  .byte cmd_d,    du_3
  .byte $20,      du_1
  .byte cmd_d,    du_3
  .byte $20,      du_1
  .byte cmd_d,    du_3
  .byte $20,      du_1
  .byte cmd_d,    du_3
  .byte $20,      du_1
  .byte cmd_d,    du_3
  .byte $20,      du_1
  .byte cmd_d,    du_3
  .byte $10,      du_1
  .byte cmd_d,    du_3
  .byte $10,      du_1
  .byte cmd_d,    du_3
  .byte $10,      du_1
  .byte cmd_d,    du_3
  .byte $10,      du_1
  .byte cmd_d,    du_3
  .byte $10,      du_1
  .byte cmd_d,    du_3
  .byte $10,      du_1
  .byte cmd_d,    du_3
  .byte $10,      du_1
  .byte cmd_d,    du_3
  .byte $10,      du_1
  .byte cmd_d,    du_3


  .byte $00
