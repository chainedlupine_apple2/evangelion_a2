import sys, os, math, struct

def pattern_has_data(pattdata):
  cnt = 0
  for i in range(0, len(pattdata), 2):
    if pattdata[i] == 0:
      break
    cnt += 1
  return cnt > 0

def seqs_has_data(seqsdata):
  cnt = 0
  for i in range(0, len(seqsdata)):
    if seqsdata[i] == 255:
      break
    cnt += 1
  return cnt > 0


def dump_seqs(fileptr, seqdata, pattnames):
  idx = 0
  lseqs = list()
  for i in range(0, len(seqdata)):
    if seqdata[i] == 255:
      break
    lseqs.append(pattnames[seqdata[i]])
    idx += 1
    if idx > 0:
      idx = 0
      strnames = ", ".join(lseqs)
      fileptr.write("\t.addr {}\n".format(strnames))
      lseqs.clear()

  if len(lseqs) > 0:
    strnames = ", ".join(lseqs)
    fileptr.write("\t.addr {}\n".format(strnames))

  fileptr.write("\t.byte $FF\n\n")

def dump_data(fileptr, data, delim = 0):
  idx = 0
  lbytes = list()
  for i in range(0, len(data)):
    if data[i] == delim:
      break
    lbytes.append(data[i])
    idx += 1
    if idx > 16:
      idx = 0
      strbytes = ", ".join(['${:02x}'.format(h) for h in lbytes])
      fileptr.write("\t.byte {}\n".format(strbytes))
      lbytes.clear()

  # write remainder
  if len(lbytes) > 0:
    strbytes = ", ".join(['${:02x}'.format(h) for h in lbytes])
    fileptr.write("\t.byte {}\n".format(strbytes))
  
# data should be a bytearray or list or some other iterable datatype
def dump_data_double(fileptr, data, delim = 0):
  idx = 0
  lbytes = list()
  for i in range(0, len(data), 2):
    if data[i] == delim:
      break
    lbytes.append(data[i])
    lbytes.append(data[i+1])
    idx += 1
    if idx > 8:
      idx = 0
      strbytes = ", ".join(['${:02x}'.format(h) for h in lbytes])
      fileptr.write("\t.byte {}\n".format(strbytes))
      lbytes.clear()

  # write remainder
  if len(lbytes) > 0:
    strbytes = ", ".join(['${:02x}'.format(h) for h in lbytes])
    fileptr.write("\t.byte {}\n".format(strbytes))
  
  fileptr.write("\t.byte $00\n\n")

# convert .bbt file to .inc for binary include
def main():
  bbtfilenm = sys.argv[1]
  incfilename = os.path.splitext(bbtfilenm)[0] + '.inc'

  with open(bbtfilenm, "rb") as inputf:
    data = inputf.read()

  header = struct.unpack("<BBBBBBHHH", data[:12])

  if header[0] is not 66 and header[1] is not 84:
    print ("Header invalid, magic bytes not found!")

  songname = struct.unpack("<8s", data[12:12+8])
  songname = songname[0].decode("ascii").split('\0', 1)[0]
  print("Converting {}...".format(songname))

  offset = 12+8

  seqs = bytearray(data[offset:offset+128])

  offset += 128
  pattdatalen = (32 * 2 * 128)
  patterns = list()

  for i in range(0, 31):
    ptr = i * 256
    pdata = bytearray(data[offset + ptr:offset + ptr + 256])
    patterns.append(pdata)

  offset += pattdatalen
  patternnames = list()

  for i in range(0, 31):
    ptr = i * 8
    pstr = struct.unpack("<8s", data[offset + ptr: offset + ptr + 8])
    pstr = pstr[0].decode("ascii").split('\0', 1)[0]
    patternnames.append(pstr)

  patternlabels = list()
  for i in range(0, 31):
    if pattern_has_data (patterns[i]):
      patternlabels.append("BBT_{}_PATT_{}".format(songname, patternnames[i]))


  with open(incfilename, "w") as outputf:
    outputf.write("; ---------------------------------------------\n")
    outputf.write("; Data for Buzzbox Tracker song {}\n".format(songname))
    outputf.write("; ---------------------------------------------\n")

    for i in range(0, 31):
      if pattern_has_data (patterns[i]):
        outputf.write(".export {}\n".format(patternlabels[i]))
    
    outputf.write("\n")

    # write out all patterns, first
    for i in range(0, 31):
      if pattern_has_data (patterns[i]):
        outputf.write("{}:\n".format(patternlabels[i]))
        dump_data_double(outputf, patterns[i])

    # now write sequences, if any are defined
    if seqs_has_data (seqs):
      outputf.write(".export BBT_{}_SEQS\n".format(songname))
      outputf.write("BBT_{}_SEQS:\n".format(songname))
      #dump_data(outputf, seqs, delim=255)
      dump_seqs(outputf, seqs, patternlabels)

if __name__ == "__main__":
  main()