.segment "CODE_H"

; music format:
; two bytes
; if first byte<$F0:
;   #0 = note frequency
;   #1 = duration (if >128, play at half volume)
; if first byte>$F0:
;   #0 = command number (minus $F0)
;   #1 = command data (varies)

no_2_C   = $C2
no_2_Cs  = $C4
no_2_D   = $C7
no_2_Ds  = $CA
no_2_E   = $C5
no_2_F   = $BF
no_2_Fs  = $BA
no_2_G   = $B3
no_2_Gs  = $AE
no_2_A   = $AA
no_2_As  = $A1
no_2_B   = $98

no_3_C   = $60
no_3_Cs  = $5A
no_3_D   = $56
no_3_Ds  = $51
no_3_E   = $48
no_3_F   = $46
no_3_Fs  = $44
no_3_G   = $40
no_3_Gs  = $3C
no_3_A   = $39
no_3_As  = $36
no_3_B   = $33

no_4_C   = $2F
no_4_Cs  = $2D
no_4_D   = $2A
no_4_Ds  = $28
no_4_E   = $26
no_4_F   = $24
no_4_Fs  = $22
no_4_G   = $20
no_4_Gs  = $1E
no_4_A   = $1C
no_4_As  = $1A
no_4_B   = $19

no_5_C   = $17
no_5_Cs  = $16
no_5_D   = $15
no_5_Ds  = $14
no_5_E   = $12
no_5_F   = $11
no_5_Fs  = $10
no_5_G   = $0F
no_5_Gs  = $0E
no_5_A   = $0D
no_5_As  = $0C
no_5_B   = $0B

no_6_C   = $0B
no_6_D   = $0A
no_6_G   = $09
no_7_F   = $08
no_7_G   = $07
no_7_A   = $06
no_8_C   = $05
no_8_Ds  = $04
no_8_G   = $03


du_1    = $10
du_2    = $20
du_3    = $30
du_4    = $40
du_5    = $50
du_6    = $60
du_7    = $6F
du_8    = $7F   ; max without cmd_d
du_9    = $FF   ; only works with cmd_d

cmd_idx = $F0
cmd_d   = cmd_idx + 1
cmd_v   = cmd_idx + 2


no_hv = $80


; arg1w = points to song data
; arg2w = per-tick callback.  In callback, set carry to stop song playback
; a = set to nonzero for callback
SUB SOUND_PLAY_SONG
    pha
    ZP_SAVE
    pla
    beq add_null
    ; not empty, so patch in
    lda arg2w
    sta callback+1
    lda arg2w+1
    sta callback+2
    jmp skip_null_callback

  add_null:
    lda #<callback_null
    sta callback+1
    lda #>callback_null
    sta callback+2

  skip_null_callback:

    Util_LOAD arg1w, temp1w

    lda #7
    sta sound_volume

  song_loop:
    ldy #0
    ; load note data
    lda (temp1w), y
    bne :+
      jmp exit
    : 

    cmp #$F0
    bcs :+
      ; if high bit not set, we are a note
      jmp do_note
    :

    ; otherwise we must be a command
    sec
    sbc #cmd_idx  ; subtract the command offset out
    cmp #1 ; cmd 1 = delay
    bne :+
      jmp cmd_delay
    :
    cmp #2 ; cmd 2 = volume
    bne :+
      jmp cmd_volume
    :

    ; unknown
    jmp advance
    
  do_note:
    sta sound_freq

    lda curr_vol
    sta sound_volume

    ; not a command, but a note
    ldy #1
    ; load duration data
    lda (temp1w),y
    tay
    and #$80
    beq skip_half_volume
      lda curr_vol
      sec
      sbc #3
      bpl :+ ; if <0, reset
        lda #1
      :
      bne :+ ; if =0, reset
        lda #1
      :
      and #$7
      sta sound_volume
      
  skip_half_volume:
    tya
    and #$7F
    sta sound_duration

    jsr PLAY_FREQ

  advance:
    Util_Inc_16_Addr temp1w, #2
  callback:
    jsr callback_null
    ;.byte $4C ; jsr
    ;.word $0000
    bcc :+
      jmp exit
    :
    jmp song_loop

  cmd_delay:
    ldy #1
    lda (temp1w), y
    jsr ROM::ROM_WAIT
    jmp advance

  cmd_volume:
    ldy #1
    lda (temp1w),y
    sta sound_volume
    sta curr_vol
    jmp advance

  callback_null:
    clc
    rts
  
  exit:
    ZP_RESTORE
    ; done

    rts

END_SUB

; arg1w = sequence storage (1-byte indexes to pattern buffer)
; arg2w = pattern buffer (32 * 256)
SUB PLAY_SEQ_INDEXES
    ZP_SAVE
    Util_LOAD #arg2w, temp1w
    
    ldy #0
  loop:
    lda (arg1w), y
    cmp #$FF
    beq done

    ; sequence to play



  done:

    ZP_RESTORE
    rts
END_SUB

; arg1w = sequence storage (2-byte array of ptrs to pattern data)
; arg2w = callback for music
; a = nonzero if using callback
SUB PLAY_SEQ_PTRS
    seqtable = temp1w
    pha
    ZP_SAVE
    pla
    sta arg3

    lda arg1w
    ldx arg1w+1
    sta seqtable
    stx seqtable+1

    ldy #0
  loop:
    lda (seqtable), y
    cmp #$FF
    beq done

    ; sequence to play
    sta arg1w
    iny
    lda (seqtable), y
    sta arg1w+1
    U_PHY
    lda arg3
    jsr SOUND_PLAY_SONG
    bcs abort
    U_PLY

    iny
    cpy #$FF
    bne loop

    jmp done

  abort:
    U_PLY

  done:
    ZP_RESTORE
    rts
END_SUB

.segment "DATA_H"

.export curr_vol

curr_vol:
  .byte 7