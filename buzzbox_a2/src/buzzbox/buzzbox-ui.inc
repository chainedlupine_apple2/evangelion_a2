.segment "CODE_H"

.define BB_VERSION "1.0"

.export SET_CURSOR, SET_ROW, PLAY_CLICK, PLAY_ERROR, PLAY_THUNK, DRAW_PATTERN_CURSOR, DRAW_NOTE, DRAW_PATTERN, DRAW_PATTERN_EXTRA, DRAW_UI_PATTERN, DRAW_UI_PATTERN_KEYS, CLEAR_PATTERN_CURSOR, CLEAR_PATTERN_LINES, CLEAR_UI_PATTERN_KEYS, CALC_PATTERN_CURSOR
.export CLEAR_PATTERN_LINES, OUTPUT_NOTE, OUTPUT_FREQ_OR_CMD, OUTPUT_DURATION_OR_VAL

; a = cursor x
; x = cursor y
SET_CURSOR:
  sta ROM::ROM_CH
  txa
  jsr SET_ROW
  rts

; a = cursor y
SET_ROW:
  sta ROM::ROM_CV
  ;jsr ROM::ROM_CURDOWN
  ;jsr ROM::ROM_CURUP
  jsr ROM::ROM_VTAB
  rts

.proc PLAY_CLICK
  ZP_SAVE
  Util_LOAD #snd_click, arg1w
  lda #0
  jsr SOUND_PLAY_SONG
  ZP_RESTORE
  rts
.endproc

.proc PLAY_THUNK
  ZP_SAVE
  Util_LOAD #snd_thunk, arg1w
  lda #0
  jsr SOUND_PLAY_SONG
  ZP_RESTORE
  rts
.endproc

.proc PLAY_ERROR
  ZP_SAVE
  Util_LOAD #snd_error, arg1w
  lda #0
  jsr SOUND_PLAY_SONG
  ZP_RESTORE
  rts
.endproc


.proc PLAY_LONG
  ZP_SAVE
  Util_LOAD #snd_long, arg1w
  lda #0
  jsr SOUND_PLAY_SONG
  ZP_RESTORE
  rts
.endproc

DRAW_PATTERN_CURSOR:
    lda pattern_cursorx
    asl
    clc
    adc pattern_cursorx
    adc #2
    sta temp1

    ldx #2
    jsr SET_CURSOR

    lda #$9D - $80
    jsr ROM::ROM_COUT
    lda #$9B - $80
    jsr ROM::ROM_COUT

    lda temp1
    ldx #8
    jsr SET_CURSOR

    lda #$9D - $80
    jsr ROM::ROM_COUT
    lda #$9B - $80
    jsr ROM::ROM_COUT

    rts

CLEAR_PATTERN_CURSOR:
    lda pattern_cursorx
    asl
    clc
    adc pattern_cursorx
    adc #2
    sta temp1

    ldx #2
    jsr SET_CURSOR

    lda #' ' + $80
    jsr ROM::ROM_COUT
    lda #' ' + $80
    jsr ROM::ROM_COUT

    lda temp1
    ldx #8
    jsr SET_CURSOR

    lda #' ' + $80
    jsr ROM::ROM_COUT
    lda #' ' + $80
    jsr ROM::ROM_COUT

    rts


; if cursor is less than offset, then move offset = cursor
; if cursor is greater than offset + 13, then move offset = cursor
; constraints:
; if offset is greater than 128-13(=115), then set it to that.

CHECK_PATTERN_SCREEN_SHIFT:
    ldx #0
    lda pattern_offset
    cmp pattern_cursor
    bcc :+
      ; cursor is less than offset
      lda pattern_cursor
      sta pattern_offset
      ldx #1
    :
    lda pattern_offset
    clc
    adc #12
    cmp pattern_cursor
    bcs :+
      ; cursor is greater than offset+13
      lda pattern_cursor
      sec
      sbc #12
      sta pattern_offset
      ldx #1
    :

    ; constraint:
    lda pattern_offset
    cmp #116
    bcc :+
      lda #116
      sta pattern_offset
    :

    cpx #1
    bne :+
      jsr ROM::WAIT_VBLANK
      jsr DRAW_PATTERN
    :

    rts


CALC_PATTERN_CURSOR:
    jsr CHECK_PATTERN_SCREEN_SHIFT

    lda pattern_cursor
    sec
    sbc pattern_offset
    sta pattern_cursorx
    rts


SUB DRAW_UI_TOP
    lda #0
    ldx #0
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL
    ROM_PRINT #txt_ui_logo

    rts
END_SUB

DRAW_UI_PATTERN_TOP:
    lda #30
    ldx #0
    JSR SET_CURSOR
    JSR ROM::ROM_CLREOL
    ROM_PRINT_CINV #txt_ui_pattern_top

    lda currpattern
    jsr ROM::ROM_PRBYTE

    ROM_PRINT_CINV #txt_ui_pattern_top_end

    rts

DRAW_UI_PATTERN:
    lda #0
    ldx #3
    JSR SET_CURSOR
    lda #'S' + $80
    jsr ROM::ROM_COUT

    JSR ROM::ROM_CURLEFT
    JSR ROM::ROM_CURDOWN
    JSR ROM::ROM_CURDOWN
    lda #'F' + $80
    jsr ROM::ROM_COUT

    JSR ROM::ROM_CURLEFT
    JSR ROM::ROM_CURDOWN
    JSR ROM::ROM_CURDOWN
    lda #'D' + $80
    jsr ROM::ROM_COUT

    jsr DRAW_UI_PATTERN_KEYS

    rts

DRAW_UI_PATTERN_KEYS:
    lda #0
    ldx #17
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL

    ROM_PRINT_CINV #txt_ui_pattern_cmds1

    ;lda ui_piano
    ;jsr ROM::ROM_HEX

    lda #0
    ldx #19
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL

    ROM_PRINT_CINV #txt_ui_pattern_cmds2

    lda #0
    ldx #21
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL

    ROM_PRINT_CINV #txt_ui_pattern_cmds3

    rts

CLEAR_UI_PATTERN_KEYS:
    ldy #12
  :
    tya
    tax
    lda #0
    U_PHY
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL
    U_PLY
    iny
    cpy #22
    bne :-

    rts

DRAW_PATTERN_EXTRA:
    ; position line
    lda #0
    ldx #10
    jsr SET_CURSOR
    ;jsr ROM::ROM_CLREOL
    ldx #8
    jsr ROM::ROM_PRBL2

    lda #0
    ldx #10
    jsr SET_CURSOR

    lda pattern_cursor
    jsr ROM::ROM_PRBYTE

    ROM_PRINT #txt_ui_pattern_len

    ;lda pattern_offset
    ;jsr ROM::ROM_PRBYTE

    lda #25
    ldx #10
    jsr SET_CURSOR

    ROM_PRINT #txt_patt_label2
    ROM_PRINT currpatternnameptr


    rts

SUB DRAW_PATTERN
    jsr DRAW_PATTERN_EXTRA
    jsr CLEAR_PATTERN_LINES

    Util_LOAD currpatternptr, temp1w
    
    ; start = offset
    ; end = offset + (128 offset - 20)

    lda #3
    sta arg4 ; cursor y

    ldy #0

  @loop:
    tya
    sta temp1
    asl ; 2x the position
    clc
    adc temp1 ; add y to get to 3x position
    adc #2 ; 2 offset on position
    sta arg3 ; cursor x
    U_PHY
    tya
    clc
    adc pattern_offset ; add in offset to y index
    asl ; 2x offset
    tay
    lda (temp1w),y
    sta arg1
    iny
    lda (temp1w),y
    sta arg2

    jsr DRAW_NOTE

    U_PLY
    iny
    cpy #13
    bcc @loop

  @exit:
    rts

END_SUB

; arg1 = note/cmd
; arg2 = duration/value
; arg3 = cursor x
; arg4 = cursor y
DRAW_NOTE:
    lda arg3
    ldx arg4
    jsr SET_CURSOR
    jsr OUTPUT_NOTE

    lda arg3
    ldx arg4
    jsr SET_CURSOR

    jsr ROM::ROM_CURDOWN
    jsr ROM::ROM_CURDOWN
    jsr OUTPUT_FREQ_OR_CMD

    lda arg3
    ldx arg4
    jsr SET_CURSOR
    
    jsr ROM::ROM_CURDOWN
    jsr ROM::ROM_CURDOWN
    jsr ROM::ROM_CURDOWN
    jsr ROM::ROM_CURDOWN
    jsr OUTPUT_DURATION_OR_VAL

    rts

; arg1 = note
OUTPUT_FREQ_OR_CMD:
    lda arg1
    bne :+
      lda #$20 + 128
      jsr ROM::ROM_COUT
      lda #$20 + 128
      jsr ROM::ROM_COUT
      rts
    :

    lda arg1
    cmp #$EF
    bcc :+
      ; we are a command
      lda #00 ; @
      jsr ROM::ROM_COUT

      lda arg1
      sec
      sbc #$F0
      clc
      adc #$30
      jsr ROM::ROM_COUT
      rts
    :
    lda arg1
    jsr ROM::ROM_PRBYTE
    rts

; arg2 = data
OUTPUT_DURATION_OR_VAL:
    lda arg1
    bne :+
      lda #$20 + 128
      jsr ROM::ROM_COUT
      lda #$20 + 128
      jsr ROM::ROM_COUT
      rts
    :

    lda arg1
    and #$80
    bne @cmd
      ; if note, check if high bit is set on arg2
      lda arg2
      and #$80
      beq :+
        lda arg2
        and #$7F
        jsr ROM::ROM_INVERSE
        jsr ROM::ROM_PRBYTE
        jsr ROM::ROM_NORMAL
        rts
      :
      lda arg2
      jsr ROM::ROM_PRBYTE
      rts

  @cmd:
    ; just print it raw
    lda arg2
    jsr ROM::ROM_PRBYTE
    rts

; arg1 = note
OUTPUT_NOTE:
    lda arg1
    and #$80
    beq :+
      rts
    :

    Util_LOAD #note_lookup_txt, temp2w    

    ; lookup the note in the table
  @loop:
    ldy #0
    lda (temp2w), y
    beq @exit

    cmp arg1
    beq :+
      jmp @next
    :

    ldy #1
    lda (temp2w), y
    ora #$80
    jsr ROM::ROM_COUT
    
    ldy #2
    lda (temp2w), y
    ;ora #$80
    jsr ROM::ROM_COUT

    jmp @exit

  @next:
    Util_Inc_16_Addr temp2w, #3
    jmp @loop

  @exit:
    rts

CLEAR_PATTERN_LINES:
    ldy #4
  @loop:
    tya
    clc
    adc #2
    tax
    lda #1
    U_PHY
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL
    U_PLY
    dey
    bne @loop
    rts


SUB MOVE_PATTERN_CURSOR_LEFT
    jsr CLEAR_PATTERN_CURSOR
    dec pattern_cursor
    bpl :+
      lda #$7F
      sta pattern_cursor
  :
    jsr CALC_PATTERN_CURSOR

    jsr DRAW_PATTERN_CURSOR
    jsr DRAW_PATTERN_EXTRA
    rts
END_SUB

SUB MOVE_PATTERN_CURSOR_RIGHT
    jsr CLEAR_PATTERN_CURSOR
    inc pattern_cursor
    lda pattern_cursor
    cmp #$7F
    bcc :+
      lda #$0
      sta pattern_cursor
  :
    jsr CALC_PATTERN_CURSOR

    jsr DRAW_PATTERN_CURSOR
    jsr DRAW_PATTERN_EXTRA
    rts
END_SUB


SUB draw_cursor
    lda #'?' + $40
    jsr ROM::ROM_COUT
    rts
END_SUB

SUB erase_cursor
    jsr ROM::ROM_CURLEFT
    lda #' ' + $80
    jsr ROM::ROM_COUT
    jsr ROM::ROM_CURLEFT
    rts
END_SUB

; a = to convert
; returns: UPPER in a
SUB CONV_TO_UPPER
    cmp #$61
    bcc exit
    cmp #$7B
    bcs exit
    ; convert to upper by masking off bit 5
    and #$DF

  exit:
    rts
END_SUB

; carry set if yes
SUB ASK_YES_NO
    inkey = temp1

    lda #20
    jsr ROM::ROM_WAIT

		sta ROM::SB_KEYB
    jsr draw_cursor

  loop:
	:	lda ROM::IN_KEYB
		bpl :-
		sta ROM::SB_KEYB

    and #$7F
    jsr CONV_TO_UPPER
    sta inkey


    lda inkey
    cmp #ROM::KB::kb_Y
    bne :+
      jsr PLAY_CLICK
      jsr erase_cursor
      sec
      rts
    :

    cmp #ROM::KB::kb_N
    bne :+
      jsr PLAY_CLICK
      jsr erase_cursor
      clc
      rts
    :

    jsr PLAY_ERROR

    jmp loop

END_SUB

.macro DRAW_LSTR8 arg
    Util_LOAD_AX_Addr arg
    jsr DRAW_8_STRING
.endmacro

; a,x = lo,hi to string data
SUB DRAW_8_STRING
    ZP_SAVE
    sta temp1w
    stx temp1w+1

    ldy #0
  loop:
    lda (temp1w), y
    beq done
    ora #$80
    jsr ROM::ROM_COUT
    iny
    cpy #8
    bcc loop

  done:
    ZP_RESTORE
    rts
END_SUB

.segment "DATA_H"

DEFDATA ui_state
  .byte 01 ; 0 = song editor, 1 = pattern editor
END_DEFDATA

.segment "RODATA_H"

.export snd_click, snd_error, snd_thunk
.export txt_ui_logo, note_lookup_txt

txt_ui_pattern_len:
  .asciiz "/7F"

txt_ui_logo:
  .byte "BuzzBox Tracker v"
  .byte BB_VERSION
  .asciiz " by CL"


snd_click:
  .byte cmd_v,    5

  .byte $50,      du_2
  .byte cmd_d,    du_3
  .byte $40,      du_1
  .byte cmd_d,    du_3
  .byte 0

snd_thunk:
  .byte cmd_v,    5

  .byte $70,      du_3
  .byte cmd_d,    du_2
  .byte $7A,      du_2
  .byte cmd_d,    du_2
  .byte 0

snd_error:
  .byte cmd_v,    7
  .byte no_3_F,   du_3
  .byte no_3_C,   du_4
  .byte 0

snd_long:
  .byte cmd_v,    7
  .byte no_4_G,   du_2
  .byte no_4_G,   du_2
  .byte no_4_G,   du_2
  .byte no_4_B,   du_3
  .byte 0

note_lookup_txt:
  .byte no_3_C,   '3', 'C' + $80
  .byte no_3_Cs,  '3', 'C' - $40
  .byte no_3_D,   '3', 'D' + $80
  .byte no_3_Ds,  '3', 'D' - $40
  .byte no_3_E,   '3', 'E' + $80
  .byte no_3_F,   '3', 'F' + $80
  .byte no_3_Fs,  '3', 'F' - $40
  .byte no_3_G,   '3', 'G' + $80
  .byte no_3_Gs,  '3', 'G' - $40
  .byte no_3_A,   '3', 'A' + $80
  .byte no_3_As,  '3', 'A' - $40
  .byte no_3_B,   '3', 'B' + $80
  
  .byte no_4_C,   '4', 'C' + $80
  .byte no_4_Cs,  '4', 'C' - $40
  .byte no_4_D,   '4', 'D' + $80
  .byte no_4_Ds,  '4', 'D' - $40
  .byte no_4_E,   '4', 'E' + $80
  .byte no_4_F,   '4', 'F' + $80
  .byte no_4_Fs,  '4', 'F' - $40
  .byte no_4_G,   '4', 'G' + $80
  .byte no_4_Gs,  '4', 'G' - $40
  .byte no_4_A,   '4', 'A' + $80
  .byte no_4_As,  '4', 'A' - $40
  .byte no_4_B,   '4', 'B' + $80

  .byte no_5_C,   '5', 'C' + $80
  .byte no_5_Cs,  '5', 'C' - $40
  .byte no_5_D,   '5', 'D' + $80
  .byte no_5_Ds,  '5', 'D' - $40
  .byte no_5_E,   '5', 'E' + $80
  .byte no_5_F,   '5', 'F' + $80
  .byte no_5_Fs,  '5', 'F' - $40
  .byte no_5_G,   '5', 'G' + $80
  .byte no_5_Gs,  '5', 'G' - $40
  .byte no_5_A,   '5', 'A' + $80
  .byte no_5_As,  '5', 'A' - $40
  .byte no_5_B,   '5', 'B' + $80
  .byte 0

