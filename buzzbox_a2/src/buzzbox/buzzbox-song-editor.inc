.segment "CODE_H"

SUB BUZZBOX_SONG_EDITOR
    jsr REDRAW_SCREEN
  loop:
    jsr HANDLE_SONG_KEYBOARD
    bcc loop

    rts

END_SUB


SUB REDRAW_SCREEN
    jsr ROM::WAIT_VBLANK
    jsr ROM::ROM_HOME

    jsr DRAW_UI_TOP
    jsr DRAW_UI_SONG_TOP
    jsr DRAW_UI_MODE_HELP

    jsr CALCULATE_PATTERN_LENS

    jsr DRAW_UI_SONG_SEQUENCES
    jsr DRAW_UI_SONG_PATTERNS

    jsr CALC_SONG_PATTERN_CURSOR
    jsr DRAW_SONG_PATTERN_CURSOR

    jsr CALC_SONG_SEQ_CURSOR
    jsr DRAW_SONG_SEQ_CURSOR

    jsr DRAW_SONG_SEQ_EXTRA
    rts
END_SUB

SUB HANDLE_SONG_KEYBOARD
    inkey = temp1
	:	lda ROM::IN_KEYB
		bpl :-
		sta ROM::SB_KEYB
		and #$7f ; mask off hi bit
    jsr CONV_TO_UPPER
		sta inkey

		; key press in temp1
		lda inkey
    cmp #ROM::KB::kb_Q
    bne :+
      jmp HANDLE_QUIT
  :

    cmp #ROM::KB::kb_R
    bne :+
      jmp HANDLE_SONG_RENAME
  :

    cmp #ROM::KB::kb_S
    bne :+
      jmp HANDLE_SONG_SAVE
  :

    cmp #ROM::KB::kb_D
    bne :+
      jmp HANDLE_DISK_MODE
    :

    cmp #ROM::KB::kb_K
    bne :+
      jmp HANDLE_KILL_SEQ
    :

    cmp #ROM::KB::kb_L
    bne :+
      jmp HANDLE_SONG_LOAD
  :
    cmp #ROM::KB::kb_left_arrow
    bne :+
      jmp HANDLE_MODE_SEQ
  :

    cmp #ROM::KB::kb_P
    bne :+
      jmp HANDLE_PLAY_WHOLE_SONG
  :

    cmp #ROM::KB::kb_right_arrow
    bne :+
      jmp HANDLE_MODE_PATT
  :

    lda song_mode
    cmp #1
    beq kb_pattern_mode
    
    ; seq commands
    lda inkey
    cmp #ROM::KB::kb_up_arrow
		bne :+
		  jmp HANDLE_SONG_SEQ_CURSOR_UP
	:
    cmp #ROM::KB::kb_down_arrow
		bne :+
		  jmp HANDLE_SONG_SEQ_CURSOR_DOWN
	:
    cmp #ROM::KB::kb_C
    bne :+
      jmp HANDLE_CLEAR_SEQS
    :

    jmp skip

  kb_pattern_mode:
    lda inkey
    cmp #ROM::KB::kb_up_arrow
		bne :+
		  jmp HANDLE_SONG_PATT_CURSOR_UP
	:
    cmp #ROM::KB::kb_down_arrow
		bne :+
		  jmp HANDLE_SONG_PATT_CURSOR_DOWN
	:
    cmp #ROM::KB::kb_enter
    bne :+
      jmp INSERT_PATTERN_AT_END
  :
    cmp #ROM::KB::kb_E
    bne :+
      jmp HANDLE_EDIT_PATTERN
    :
    cmp #ROM::KB::kb_I
    bne :+
      jmp INSERT_PATTERN_AT_CURSOR
    :

  skip:
    ; draw unknown key for debugging
    ;lda #10
    ;ldx #15
    ;jsr SET_CURSOR
    ;lda inkey
    ;jsr ROM::ROM_PRBYTE

    clc
    rts
END_SUB

SUB SONG_HANDLE_EXIT
    clc
    rts
END_SUB

SUB HANDLE_DISK_MODE
    jsr DO_DISK
    jsr REDRAW_SCREEN
    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_PLAY_WHOLE_SONG
    jsr CLEAR_UI_MODE_HELP
    jsr CALCULATE_SEQ_PTRS
    Util_LOAD #seqptrstorage, arg1w
    Util_LOAD #MUSIC_KEYBOARD_ABORT_CALLBACK, arg2w
    lda #20
    sta arg3 ; just arg3 for a skip count so we're not doing a keyboard poll every tick (slows down music)
    jsr PLAY_SEQ_PTRS
    jsr DRAW_UI_MODE_HELP
    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_QUIT
    jsr CLEAR_UI_MODE_HELP

    lda #7
    ldx #4
    jsr SET_CURSOR

    ROM_PRINT #txt_song_quit_label

    jsr ASK_YES_NO
    bcc :+
      sec
      rts ; first exit HANDLE_SONG_KEYBOARD
    :

    jsr DRAW_UI_MODE_HELP

    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_CLEAR_SEQS
    jsr CLEAR_UI_MODE_HELP

    lda #7
    ldx #4
    jsr SET_CURSOR

    ROM_PRINT #txt_song_clear_label

    jsr ASK_YES_NO
    bcc :+
      jsr CLEAR_ALL_SEQUENCES

      jsr PLAY_LONG
      jsr DRAW_UI_SONG_SEQUENCES
    :

    jsr DRAW_UI_MODE_HELP
    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_KILL_SEQ
    lda song_seq_cursor
    jsr DELETE_SEQ_AT
    jsr DRAW_UI_SONG_SEQUENCES
    jsr PLAY_CLICK

    jmp SONG_HANDLE_EXIT
END_SUB

SUB INSERT_PATTERN_AT_END
    ldx #0
  loop:
    lda seqstorage, x
    cmp #$FF
    beq foundempty
    inx
    cpx #$80
    bcc loop

  foundempty:
    cpx #$7F
    beq error

    lda song_pattern_cursor
    sta seqstorage, x

    jsr DRAW_UI_SONG_SEQUENCES
    jmp exit

  error:
    jsr PLAY_ERROR

  exit:
    jsr PLAY_CLICK

    jmp SONG_HANDLE_EXIT
END_SUB

SUB INSERT_PATTERN_AT_CURSOR
    lda song_seq_cursor
    jsr INSERT_BLANK_SEQ_AT

    ldx song_seq_cursor
    lda song_pattern_cursor
    sta seqstorage, x

    jsr DRAW_UI_SONG_SEQUENCES
    jsr PLAY_CLICK

    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_MODE_SEQ
    lda #0
    sta song_mode
    jsr DRAW_SONG_PATTERN_CURSOR
    jsr DRAW_SONG_SEQ_CURSOR
    jsr DRAW_UI_MODE_HELP
    
    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_MODE_PATT
    lda #1
    sta song_mode
    jsr DRAW_SONG_PATTERN_CURSOR
    jsr DRAW_SONG_SEQ_CURSOR
    jsr DRAW_UI_MODE_HELP

    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_EDIT_PATTERN
    jsr PLAY_CLICK
    ZP_SAVE
    lda song_pattern_cursor
    sta currpattern      
    jsr BUZZBOX_PATTERN_EDITOR
    ZP_RESTORE
    jsr REDRAW_SCREEN
    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_SONG_SAVE
    jsr CLEAR_UI_MODE_HELP

    lda #7
    ldx #4
    jsr SET_CURSOR

    ROM_PRINT #txt_save_filename

    jsr GET_FILENAME
    bcs :+
      jsr SAVE_SONG    
    :

    jsr REDRAW_SCREEN
    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_SONG_LOAD
    jsr CLEAR_UI_MODE_HELP

    lda #7
    ldx #4
    jsr SET_CURSOR

    ROM_PRINT #txt_load_filename

    jsr GET_FILENAME
    bcs :+
      jsr LOAD_SONG 
      lda #0
      sta song_pattern_cursor
      sta song_seq_cursor 
    :

    jsr REDRAW_SCREEN
    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_SONG_RENAME
    jsr CLEAR_UI_MODE_HELP

    lda #7
    ldx #4
    jsr SET_CURSOR
    ROM_PRINT #txt_song_rename_label

    jsr UI_ENTER_STRING
    bcs :+
      ; have string name, now replace

      ldx #0
    loop:
      lda textinput_buffer, x
      sta songname, x
      inx
      cpx #8
      bcc loop
    :

    jsr DRAW_UI_MODE_HELP    
    jsr DRAW_UI_SONG_TOP
    jmp SONG_HANDLE_EXIT
END_SUB


SUB HANDLE_SONG_PATT_CURSOR_UP
    jsr CLEAR_SONG_PATTERN_CURSOR
    lda song_pattern_cursor
    beq :+
    dec song_pattern_cursor
    jmp :++
  :
    lda #$20-1
    sta song_pattern_cursor
  :
    jsr CALC_SONG_PATTERN_CURSOR

    jsr DRAW_SONG_PATTERN_CURSOR
    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_SONG_PATT_CURSOR_DOWN
    jsr CLEAR_SONG_PATTERN_CURSOR
    inc song_pattern_cursor
    lda song_pattern_cursor
    cmp #$20
    bcc :+
    lda #0
    sta song_pattern_cursor
  :
    jsr CALC_SONG_PATTERN_CURSOR

    jsr DRAW_SONG_PATTERN_CURSOR

    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_SONG_SEQ_CURSOR_UP
    jsr CLEAR_SONG_SEQ_CURSOR
    lda song_seq_cursor
    beq :+
    dec song_seq_cursor
    jmp :++
  :
    lda #$80-1
    sta song_seq_cursor
  :
    jsr CALC_SONG_SEQ_CURSOR

    jsr DRAW_SONG_SEQ_CURSOR
    jsr DRAW_SONG_SEQ_EXTRA
    jmp SONG_HANDLE_EXIT
END_SUB

SUB HANDLE_SONG_SEQ_CURSOR_DOWN
    jsr CLEAR_SONG_SEQ_CURSOR
    inc song_seq_cursor
    lda song_seq_cursor
    cmp #$80
    bcc :+
    lda #0
    sta song_seq_cursor
  :
    jsr CALC_SONG_SEQ_CURSOR

    jsr DRAW_SONG_SEQ_CURSOR
    jsr DRAW_SONG_SEQ_EXTRA
    jmp SONG_HANDLE_EXIT
END_SUB


.segment "RODATA_H"

DEFDATA txt_song_name_label
  .asciiz "SONG NAME: "
END_DEFDATA

DEFDATA txt_seq_label
  .asciiz "SEQ"
END_DEFDATA

DEFDATA txt_patt_label1
  .asciiz "P#"
END_DEFDATA

DEFDATA txt_patt_label2
  .asciiz "P.NAME "
END_DEFDATA

DEFDATA txt_patt_label3
  .asciiz "LEN"
END_DEFDATA

DEFDATA txt_song_ui_help1
    INVBYTE '<'
    .byte "/"
    INVBYTE '>'
    .byte "=SEQ/PATT MODE"
    .byte 0
END_DEFDATA

DEFDATA txt_song_ui_help2
    INVBYTE 'U'
    .byte "/"
    INVBYTE 'D'
    .byte "=CURSOR  "
    INVBYTE 'P'
    .byte "LAY "
    INVBYTE 'D'
    .byte "ISK "
    .byte 0
END_DEFDATA

DEFDATA txt_song_ui_help3
    INVBYTE 'S'
    .byte "AVE "
    INVBYTE 'L'
    .byte "OAD "
    INVBYTE 'R'
    .byte "ENAME "
    INVBYTE 'Q'
    .byte "UIT "
    .byte 0
END_DEFDATA

DEFDATA txt_song_rename_label
    .asciiz "SONG NAME? (ESC=abort) "
END_DEFDATA

DEFDATA txt_song_clear_label
    .asciiz "CLEAR ALL SEQUENCES? "
END_DEFDATA

DEFDATA txt_song_quit_label
    .asciiz "QUIT TO PRODOS? "
END_DEFDATA

.segment "DATA_H"
; 0=seq, 1=patt
DEFDATA song_mode
  .byte 1
END_DEFDATA

; holds length of each pattern
DEFDATA patternslen
  .res 32
END_DEFDATA

DEFDATA song_seq_cursor
  .byte 0
END_DEFDATA

DEFDATA song_seq_offset
  .byte 0
END_DEFDATA

DEFDATA song_seq_cursorx
  .byte 0
END_DEFDATA

DEFDATA song_pattern_cursor
  .byte 0
END_DEFDATA

DEFDATA song_pattern_offset
  .byte 0
END_DEFDATA

DEFDATA song_pattern_cursorx
  .byte 0
END_DEFDATA
