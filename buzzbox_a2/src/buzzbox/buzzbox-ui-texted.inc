.segment "CODE_H"

.export UI_ENTER_NUMBER, UI_ENTER_ALPHA, UI_ENTER_STRING, UI_ENTER_NUMBER_SINGLE, UI_ENTER_PREFIX

string_type_number  = $0
string_type_alpha   = $1
string_type_path    = $2

enter_mode_empty      = $0
enter_mode_prefilled  = $1

UI_ENTER_NUMBER:
    ZP_SAVE
    lda #2
    sta arg1
    lda #string_type_number
    sta arg2
    lda #enter_mode_empty
    sta arg3

    jsr UI_ENTER_ALPHA
    ZP_RESTORE
    rts

UI_ENTER_NUMBER_SINGLE:
    ZP_SAVE
    lda #1
    sta arg1
    lda #string_type_number
    sta arg2
    lda #enter_mode_empty
    sta arg3

    jsr UI_ENTER_ALPHA
    ZP_RESTORE
    rts

UI_ENTER_STRING:
    ZP_SAVE
    lda #8
    sta arg1
    lda #string_type_alpha
    sta arg2
    lda #enter_mode_empty
    sta arg3

    jsr UI_ENTER_ALPHA
    ZP_RESTORE
    rts

UI_ENTER_STRING_PREFILLED:
    ZP_SAVE
    lda #8
    sta arg1
    lda #string_type_alpha
    sta arg2
    lda #enter_mode_prefilled
    sta arg3

    jsr UI_ENTER_ALPHA
    ZP_RESTORE
    rts


UI_ENTER_PREFIX:
    ZP_SAVE
    lda #40
    sta arg1
    lda #string_type_path
    sta arg2
    lda #enter_mode_empty
    sta arg3

    jsr UI_ENTER_ALPHA
    ZP_RESTORE
    rts

; arg1 = length of data to enter
; arg2 = 0=numbers only, 1=letters+numbers, 2=letters+numbers+/
; arg3 = 0=empty on start, 1=use whatever is in textinput_entered
; return: carry set if error/abort, clear if OK and something entered
; if carry set, check A: 0=escape pushed, 1=enter pushed on blank line
.proc UI_ENTER_ALPHA
    inlen     = arg1
    intype    = arg2  ; 0=numbers, 1=alpha, 2=path
    inmode    = arg3  ; 0=empty, 1=prefilled
    incurrkey = temp1
    incursor  = temp2
    inaccum   = temp3

    ; clear textinput_number
    lda #0
    sta textinput_number      ; 16-bit number storage
    sta textinput_number+1    ; 16-bit number storage
    sta incurrkey             ; current key entered
    sta inaccum               ; used by alpha-to-number converter

    lda inmode
    bne :+
      lda #0
      sta textinput_entered     ; # of chars entered so far
      sta incursor              ; current cursor position
      jmp start
    :
    lda textinput_entered
    sta incursor
    ; print out what we have already
    ROM_LSTR_PRINT #textinput_entered
    ; skip over string clear
    jmp enterloop

  start:
    ldx #0
  clrloop:
    lda #0
    sta textinput_buffer,x
    inx
    cpx #8
    bcc clrloop

  enterloop:
    jsr draw_cursor
		sta $c010 ; clear strobe

	:	lda ROM::IN_KEYB
		bpl :-
		and #$7f
    jsr CONV_TO_UPPER
		sta incurrkey
		sta ROM::SB_KEYB

		; key press in temp1
		;lda incurrkey
    ;jsr ROM_PRBYTE

    ; see if escape
    lda incurrkey
    cmp #ROM::KB::kb_escape
    bne :+
      jsr erase_cursor
      lda #0 ; escape pushed
      sec
      rts
  :
    ; see if enter
    cmp #ROM::KB::kb_enter
    bne :+
      jmp endprocessing
  :

    ; see if backspace
    cmp #ROM::KB::kb_delete
    bne :+
      jmp backspace
  :
    cmp #ROM::KB::kb_left_arrow
    bne :+
      jmp backspace
  :

    ; see if at limit
    ldy incursor
    cpy inlen
    bcc :+
      jsr erase_cursor
      ; at limit
      jsr PLAY_ERROR
      jmp enterloop
    :

    lda intype
    bne @skipnumbervalidation
    ; first we do number validation
    ; now handle key press for numbers (ascii $30-$39 for 0-9, and $41 to $46 for A-F)
    lda incurrkey
    cmp #$2F
    bcc :+
    cmp #$3A
    bcs :+
    jmp @isvalid
  :
    ; don't abort yet, check to see if it's A-F
    cmp #$40
    bcc @notvalid
    cmp #$47
    bcs @notvalid

  @skipnumbervalidation:
    ; do string validation instead of numbers
    ; first check for A-Z
    lda incurrkey
    cmp #$40
    bcc :+
    cmp #$5B
    bcs :+
    jmp @isvalid
  :
    lda intype
    cmp #string_type_path
    bne :+
      ; if intype=2, check to see if we've entered $2F (forward slash) or $2E (dot)
      lda incurrkey
      cmp #$2F
      beq @isvalid
      cmp #$2E
      beq @isvalid
  :
    ; check to see if we're a number
    lda incurrkey
    cmp #$30
    bcc @notvalid
    cmp #$3A
    bcs @notvalid

  
  @isvalid:
    jsr erase_cursor

    ; is a valid character
    lda incurrkey
    ldy incursor
    sta textinput_buffer, y ; save value
    ora #$80
    jsr ROM::ROM_COUT
    inc incursor

    jsr PLAY_CLICK

    jmp enterloop

  @notvalid: ; not a valid character
    jsr erase_cursor
    jsr PLAY_ERROR

    jmp enterloop

  backspace:
    ; we are leftarrow OR delete
    lda incursor
    bne :+
      jsr erase_cursor
      jsr PLAY_ERROR
      jmp :++
  :
    lda #0
    ldy incursor
    sta textinput_buffer, y

    dec incursor
    jsr erase_cursor
    jsr erase_cursor
    jsr PLAY_THUNK
  :
    jmp enterloop

  endprocessing:
      ; save our actual length
      lda incursor
      sta textinput_entered
      bne :+
        ; no text, abort with error
        jsr erase_cursor

        lda #1 ; enter pushed on blank text
        sec
        rts
    :

      lda intype
      beq :+
        jmp @notnumber
      :
        ; do string number processing
        ; two indexes:
        ;   textinput_buffer_index, advances left to right
        ;   textinput_number_index, advances left to right

        ; start at textinput_buffer_index=0
        ; if we find value, convert ascii '0-F' to $0-$F
        ; shift value left by how many textinput_buffer indexes we've advanced.  Odd=0, Even=4
        ; add to inaccum
        ; if textinput_buffer_index % 2 = 0, then store inaccum to textinput_number[textinput_buffer_index] (to handle 16-bit or higher numbers), clear inaccum, textinput_number_index++
        ; textinput_buffer_index++, loop

        ; even better:  Right-justifying our input data lets us assume that a byte is every 2 values in string buffer.
        ; As in: ie: "01" vs "1"
        ; even bettererer:  Don't right-justify, and if it's one character short just don't shift the first byte <<4.

        ; start at textinput_buffer_index=0
        ; if we find value, convert to $0-$F
        ; shift value by left 4x, add to inaccum
        ; if next value in textinput_buffer, advance textinput_buffer_index and then load it
        ; convert to $0-$F
        ; add to inaccum
        ; store inaccum in textinput_number[textinput_buffer_index], textinput_number_index++
        ; loop

        ldx #0  ; index for textinput_buffer
        ldy #0  ; index to textinput_number
        
        
    @scanloop:
        lda #0
        sta inaccum

        lda textinput_buffer, x
        jsr convertasciitohex
        
        ;phx
        ;phy
        ;pha
        ;jsr ROM_PRBYTE
        ;pla
        ;ply
        ;plx

        ; see if we can move to the next char
        inx
        cpx textinput_entered
        bne :+
          ; we can't, just store what we got and abort run
          sta textinput_number, y
          jmp @exit
      :

        ; shift left 4x
        asl
        asl
        asl
        asl
        clc
        adc inaccum
        sta inaccum


        lda textinput_buffer, x
        jsr convertasciitohex

        ;phx
        ;phy
        ;pha
        ;jsr ROM_PRBYTE
        ;pla
        ;ply
        ;plx

        ; just add to accum
        clc
        adc inaccum

        ; now we have a full byte in accum        
        ; write to textinput_number accumulator
        sta textinput_number, y

        ; move to next textinput_number byte
        iny

        ; if we are not at end, then start over
        inx
        cpx textinput_entered
        bne @scanloop 

    @notnumber:
        ; process as string

    @exit: ; all OK
        jsr erase_cursor

        lda #0
        clc
        rts

  ; a = ascii to convert
  ; returns: a as $0-$F
  convertasciitohex:
      ; ascii 0-9 is $30-$39
      cmp #$2F
      bcc :+
      cmp #$3A
      bcs :+
      ; must be within 0-9
      sec
      sbc #$30
      rts
    :
      ; ascii A-F is $41-$46
      cmp #$40
      bcc :+
      cmp #$47
      bcs :+
      ; must be within A-F
      sec
      sbc #$41
      clc
      adc #$0A
      rts
    :
      lda #0
      rts


.endproc

.segment "DATA_H"

.export textinput_buffer, textinput_entered, textinput_number

DEFDATA textinput_entered
  .byte $0
END_DEFDATA

DEFDATA textinput_buffer
  .res 64
END_DEFDATA

DEFDATA textinput_number
  .word $0000
END_DEFDATA

