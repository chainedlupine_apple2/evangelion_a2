
SUB DRAW_UI_SONG_TOP
    lda #20
    ldx #2
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL

    ROM_PRINT #txt_song_name_label

    DRAW_LSTR8 #songname

    lda #2
    ldx #2
    jsr SET_CURSOR

    ROM_PRINT #txt_seq_label

    lda #20
    ldx #12
    jsr SET_CURSOR

    ROM_PRINT #txt_patt_label1

    lda #23
    ldx #12
    jsr SET_CURSOR

    ROM_PRINT #txt_patt_label2



    rts
END_SUB

SUB CLEAR_UI_MODE_HELP
    ldy #9
  loop:
    tya
    U_PHY
    clc
    adc #2
    tax
    lda #5
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL
    U_PLY
    dey
    bne loop
    rts
END_SUB

SUB DRAW_UI_MODE_HELP
    jsr CLEAR_UI_MODE_HELP
    
    lda #36
    ldx #12
    jsr SET_CURSOR

    ROM_PRINT #txt_patt_label3

    lda #15
    ldx #4
    jsr SET_CURSOR
    ROM_PRINT_CINV #txt_song_ui_help1

    lda #15
    ldx #5
    jsr SET_CURSOR
    ROM_PRINT_CINV #txt_song_ui_help2

    lda #15
    ldx #6
    jsr SET_CURSOR
    ROM_PRINT_CINV #txt_song_ui_help3

    lda song_mode
    cmp #0
    beq draw_seq_help
    cmp #1
    beq draw_patt_help

    rts

  mode_help_height = 8
  draw_patt_help:
    ; pattern mode
    lda #15
    ldx #mode_help_height
    jsr SET_CURSOR
    ROM_PRINT_CINV #txt_song_ui_mode_patt1

    lda #15
    ldx #mode_help_height+1
    jsr SET_CURSOR
    ROM_PRINT_CINV #txt_song_ui_mode_patt2

    lda #15
    ldx #mode_help_height+2
    jsr SET_CURSOR
    ROM_PRINT_CINV #txt_song_ui_mode_patt3
    rts


  draw_seq_help:
    ; seq mode
    lda #15
    ldx #mode_help_height
    jsr SET_CURSOR
    ROM_PRINT_CINV #txt_song_ui_mode_seq1

    lda #15
    ldx #mode_help_height+1
    jsr SET_CURSOR
    ROM_PRINT_CINV #txt_song_ui_mode_seq2

    rts

END_SUB

SUB DRAW_SONG_PATTERN_CURSOR
    lda song_pattern_cursorx
    sec
    adc #12
    sta temp1
    ldx temp1
    lda #18
    jsr SET_CURSOR

    lda song_mode
    beq inactive

    lda #$9D - $80
    jsr ROM::ROM_COUT
    lda #$BE - $80
    jsr ROM::ROM_COUT
    rts

  inactive:
    lda #$AD
    jsr ROM::ROM_COUT
    lda #$BE
    jsr ROM::ROM_COUT
    rts

END_SUB

SUB CLEAR_SONG_PATTERN_CURSOR
    lda song_pattern_cursorx
    sec
    adc #12
    sta temp1
    ldx temp1
    lda #18
    jsr SET_CURSOR

    lda #' ' + $80
    jsr ROM::ROM_COUT
    lda #' ' + $80
    jsr ROM::ROM_COUT
    rts
END_SUB 

SUB DRAW_SONG_SEQ_CURSOR
    lda song_seq_cursorx
    sec
    adc #2
    sta temp1
    ldx temp1
    lda #0
    jsr SET_CURSOR

    lda song_mode
    bne inactive

    lda #$9D - $80
    jsr ROM::ROM_COUT
    lda #$BE - $80
    jsr ROM::ROM_COUT
    rts

  inactive:
    lda #$AD
    jsr ROM::ROM_COUT
    lda #$BE
    jsr ROM::ROM_COUT
    rts

END_SUB

SUB CLEAR_SONG_SEQ_CURSOR
    lda song_seq_cursorx
    sec
    adc #2
    sta temp1
    ldx temp1
    lda #0
    jsr SET_CURSOR

    lda #' ' + $80
    jsr ROM::ROM_COUT
    lda #' ' + $80
    jsr ROM::ROM_COUT
    rts
END_SUB 


pattern_scrn_len = 10
pattern_max_len = 32
SUB CHECK_SONG_PATTERN_SCREEN_SHIFT
    ldx #0  ; flag to refresh the display
    lda song_pattern_offset
    cmp song_pattern_cursor
    bcc :+
      ; cursor is less than offset
      lda song_pattern_cursor
      sta song_pattern_offset
      ldx #1 ; refresh
    :
    lda song_pattern_offset
    clc
    adc #pattern_scrn_len-1
    cmp song_pattern_cursor
    bcs :+
      ; cursor is greater than offset+20
      lda song_pattern_cursor
      sec
      sbc #pattern_scrn_len-1
      sta song_pattern_offset
      ldx #1 ; refresh
    :

    ; constraint:
    lda song_pattern_offset
    cmp #pattern_max_len - pattern_scrn_len + 1
    bcc :+
      lda #pattern_max_len - pattern_scrn_len + 1
      sta song_pattern_offset
    :

    cpx #1
    bne :+ ; refresh requested
      jsr ROM::WAIT_VBLANK
      jsr DRAW_UI_SONG_PATTERNS
    :

    rts
END_SUB

SUB CALC_SONG_PATTERN_CURSOR
    jsr CHECK_SONG_PATTERN_SCREEN_SHIFT

    lda song_pattern_cursor
    sec
    sbc song_pattern_offset
    sta song_pattern_cursorx
    rts
END_SUB


seq_scrn_len = 20
seq_max_len = 128
SUB CHECK_SONG_SEQ_SCREEN_SHIFT
    ldx #0  ; flag to refresh the display
    lda song_seq_offset
    cmp song_seq_cursor
    bcc :+
      ; cursor is less than offset
      lda song_seq_cursor
      sta song_seq_offset
      ldx #1 ; refresh
    :
    lda song_seq_offset
    clc
    adc #seq_scrn_len-1
    cmp song_seq_cursor
    bcs :+
      ; cursor is greater than offset+20
      lda song_seq_cursor
      sec
      sbc #seq_scrn_len-1
      sta song_seq_offset
      ldx #1 ; refresh
    :

    ; constraint:
    lda song_seq_offset
    cmp #seq_max_len - seq_scrn_len + 1
    bcc :+
      lda #seq_max_len - seq_scrn_len + 1
      sta song_seq_offset
    :

    cpx #1
    bne :+ ; refresh requested
      jsr ROM::WAIT_VBLANK
      jsr DRAW_UI_SONG_SEQUENCES
    :

    rts
END_SUB

SUB CALC_SONG_SEQ_CURSOR
    jsr CHECK_SONG_SEQ_SCREEN_SHIFT

    lda song_seq_cursor
    sec
    sbc song_seq_offset
    sta song_seq_cursorx
    rts
END_SUB

SUB DRAW_UI_SONG_PATTERNS
    stroffset = temp1
    pcursory  = temp2
    pnameptr  = temp1w

    ldx song_pattern_offset
    ldy #0
  drawloop:
    U_PHX
    tya
    clc
    adc #13
    tax
    stx pcursory
    lda #20
    jsr SET_CURSOR
    U_PLX
    ; draw pattern #
    tya
    clc
    adc song_pattern_offset
    jsr ROM::ROM_PRBYTE


    ; draw name
    jsr ROM::ROM_CURRIGHT
    tya
    clc
    adc song_pattern_offset
    asl
    asl
    asl ; 8x
    sta stroffset
    U_PHX
    U_PHY
    Util_LOAD #patternnames, pnameptr
    Util_Inc_16_Addr pnameptr, stroffset
    Util_LOAD_AX_Addr pnameptr
    jsr DRAW_8_STRING
    U_PLY
    U_PLX


    ; draw len
    U_PHX
    ldx pcursory
    lda #35
    jsr SET_CURSOR
    U_PLX

    jsr ROM::ROM_CURRIGHT
    tya
    clc
    adc song_pattern_offset
    tax
    lda patternslen, x
    jsr ROM::ROM_PRBYTE
    jsr ROM::ROM_CURRIGHT

    iny
    inx
    cpy #10
    bcs exit
    jmp drawloop
  exit:


    rts
END_SUB

SUB DRAW_SONG_SEQ_EXTRA
    lda #6
    ldx #2
    jsr SET_CURSOR

    lda song_seq_cursor
    jsr ROM::ROM_PRBYTE

    ROM_PRINT #txt_song_ui_seq_extra_end
    rts
END_SUB

SUB DRAW_UI_SONG_SEQUENCES
    jsr CLEAR_UI_SONG_SEQUENCES

    ldx song_seq_offset
    ldy #3
  drawloop:
    U_PHX
    tya
    tax
    lda #2
    jsr SET_CURSOR
    U_PLX
    lda seqstorage, x
    cmp #$FF
    beq empty
    jsr ROM::ROM_PRBYTE
    jmp next
  empty:
    lda #'-' + $80
    jsr ROM::ROM_COUT
    lda #'-' + $80
    jsr ROM::ROM_COUT
  next:
    inx
    iny
    cpy #23
    bcc drawloop

    rts
END_SUB

SUB CLEAR_UI_SONG_PATTERNS
    ldy #13
  loop:
    tya
    tax
    lda #20
    jsr SET_CURSOR
    ldx #20
    jsr ROM::ROM_PRBL2
    iny
    cpy #23
    bcc loop

    rts
END_SUB

SUB CLEAR_UI_SONG_SEQUENCES
    ldy #3
  loop:
    tya
    tax
    lda #2
    jsr SET_CURSOR
    ldx #3
    jsr ROM::ROM_PRBL2
    iny
    cpy #22
    bcc loop

    rts
END_SUB


SUB CALCULATE_PATTERN_LENS
    count = temp1
    ; loop through patterns, count number of non-zero commands
    Util_LOAD #patternstorage, temp1w

    ldx #0
  loop1:
    lda #0
    sta count

    ldy #0
  loop2:
    lda (temp1w), y
    beq donepattern
    inc count
    iny
    iny
    bne loop2
  donepattern:
    lda count
    sta patternslen, x

    ;Util_Inc_16_Addr temp1w, #$FF
    inc temp1w+1

    inx
    cpx #$20
    bcc loop1

    rts
END_SUB


.segment "RODATA_H"

DEFDATA txt_song_ui_mode_patt1
    INVBYTE 'E'
    .byte "DIT PATTERN"
    .byte 0
END_DEFDATA

DEFDATA txt_song_ui_mode_patt2
    INVBYTE 'I'
    .byte "NSERT/"
    INVBYTE 'K'
    .byte "ILL AT CURSOR"
    .byte 0
END_DEFDATA

DEFDATA txt_song_ui_mode_patt3
    INVBYTE 'E'
    INVBYTE 'N'
    INVBYTE 'T'
    INVBYTE 'E'
    INVBYTE 'R'
    .byte "=ADD AT END"
    .byte 0
END_DEFDATA


DEFDATA txt_song_ui_mode_seq1
    INVBYTE 'K'
    .asciiz "ILL SEQ"
END_DEFDATA

DEFDATA txt_song_ui_mode_seq2
    INVBYTE 'C'
    .asciiz "LEAR ALL"
END_DEFDATA

DEFDATA txt_song_ui_seq_extra_end
    .asciiz "/7F"
END_DEFDATA