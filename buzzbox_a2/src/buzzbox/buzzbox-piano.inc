.segment "CODE_H"

.export STARTUP_PIANO, SHUTDOWN_PIANO, DO_PIANO, DRAW_UI_PIANO, DRAW_UI_PIANO_EXTRA

STARTUP_PIANO:
    jsr CLEAR_UI_PATTERN_KEYS
    jsr DRAW_UI_PIANO
    jsr DRAW_UI_PIANO_EXTRA
    rts

SHUTDOWN_PIANO:
    jsr CLEAR_UI_PATTERN_KEYS
    jsr DRAW_UI_PATTERN_KEYS
    rts

; you need to set ui_piano_[volume|duration|octave] first
.proc DO_PIANO

    inkey   = temp1
    note    = temp2

  keywait:
	:	lda ROM::IN_KEYB
		bpl :-

    and #$7F
    sta inkey

		sta ROM::SB_KEYB

    ;jsr ROM_PRBYTE

    lda inkey
    cmp #ROM::KB::kb_escape
    bne :+
      sec
      rts
  :
    cmp #ROM::KB::kb_enter
    bne :+
      sec
      rts
  :

    cmp #ROM::KB::kb_1
    bne :+
      lda #du_1
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_2
    bne :+
      lda #du_2
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_3
    bne :+
      lda #du_3
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_4
    bne :+
      lda #du_4
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_5
    bne :+
      lda #du_5
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_6
    bne :+
      lda #du_6
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_7
    bne :+
      lda #du_7
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_8
    bne :+
      lda #du_8
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_plus  ; vol up
    bne :++
      inc ui_piano_volume      
      lda ui_piano_volume
      cmp #7
      bcc :+
        lda #7
        sta ui_piano_volume
      :
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
  :
    
    cmp #ROM::KB::kb_minus  ; vol down
    bne :++
      dec ui_piano_volume
      bne :+
        lda #1
        sta ui_piano_volume
      :
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
  :

    cmp #ROM::KB::kb_up_arrow  ; octave up
    bne :++
      inc ui_piano_octave
      lda ui_piano_octave
      cmp #6
      bcc :+
        lda #6
        sta ui_piano_octave
      :
      jsr DRAW_UI_PIANO_EXTRA
      jsr patch_octave_lookup

      jmp keywait
  :

    cmp #ROM::KB::kb_down_arrow  ; octave down
    bne :++
      dec ui_piano_octave
      bpl :+
        lda #0
        sta ui_piano_octave
      :
      jsr DRAW_UI_PIANO_EXTRA
      jsr patch_octave_lookup
      
      jmp keywait
  :

    cmp #ROM::KB::kb_LEFTBRACKET  ; duration less
    bne :+++
      lda ui_piano_duration
      sec
      sbc #10
      ; if no carry, then we wrapped around
      bcs :+
        lda #1
      :
      bne :+
        lda #1
      :
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
  :

    cmp #ROM::KB::kb_RIGHTBRACKET  ; duration more
    bne :++
      lda ui_piano_duration
      clc
      adc #10
      cmp #$80
      ; if carry, then we are greater-than-equal
      bcc :+
        lda #$7F
      :
      sta ui_piano_duration
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
  :

    cmp #ROM::KB::kb_left_arrow
    bne :+
      jsr MOVE_PATTERN_CURSOR_LEFT
      lda pattern_cursor
      sta ui_piano_original_cursor
      jmp keywait
  :

    cmp #ROM::KB::kb_right_arrow
    bne :+
      jsr MOVE_PATTERN_CURSOR_RIGHT
      lda pattern_cursor
      sta ui_piano_original_cursor
      jmp keywait
  :

    lda ui_piano_insert_mode
    bne :+
      jmp @skip_insert_commands
    :

    lda inkey
    cmp #ROM::KB::kb_L
    bne :+
      jsr PIANO_INSERT_DELAY
      jsr PLAY_CLICK
      jmp keywait
  :

    cmp #ROM::KB::kb_K
    bne :+
      lda pattern_cursor
      jsr DELETE_PATTERN_NOTE
      jsr DRAW_PATTERN
      jsr PLAY_THUNK
      jmp keywait
  :

    cmp #ROM::KB::kb_N
    bne :+
      jsr CLEAR_PATTERN_CURSOR

      ; restore cursor position
      lda ui_piano_original_cursor
      sta pattern_cursor
      
      ; restore pattern
      jsr RESTORE_PATTERN

      jsr CALC_PATTERN_CURSOR
      jsr DRAW_PATTERN_CURSOR
      jsr DRAW_PATTERN_EXTRA
      jsr DRAW_PATTERN

      jsr PLAY_LONG
      jmp keywait
  :

  @skip_insert_commands:
    lda inkey
    cmp #ROM::KB::kb_P
    bne :+
      jsr CLEAR_UI_PATTERN_KEYS
      jsr PLAY_PATTERN_WITH_CALLBACK
      jsr DRAW_UI_PIANO
      jsr DRAW_UI_PIANO_EXTRA
      jmp keywait
  :

    ; check to see if it's a piano key
    ldx #0
  @note_scan:
    txa
    asl  ; 2x
    tay
    lda piano_matrix, y
    cmp inkey
    bne :++
      ; found
      lda piano_matrix+1, y
      jsr lookup_note
      lda ui_piano_freq
      beq :+
        sta sound_freq
        lda ui_piano_duration
        sta sound_duration
        lda ui_piano_volume
        sta sound_volume
        jsr PLAY_FREQ
        jmp exit
      :
      jmp @exit_scan
  :
    inx
    cpx #12
    bcc @note_scan

  @exit_scan:
    jmp keywait


  lookup_note:
      tay
    fixup_lookupnote:
      lda piano_octave_4, y
      sta ui_piano_freq

      rts

  patch_octave_lookup:
      lda ui_piano_octave
      bne :+ ; octave 2
        lda #<piano_octave_2
        sta fixup_lookupnote+1
        lda #>piano_octave_2
        sta fixup_lookupnote+2
        rts
      :
      cmp #1
      bne :+ ; octave 3
        lda #<piano_octave_3
        sta fixup_lookupnote+1
        lda #>piano_octave_3
        sta fixup_lookupnote+2
        rts
      :
      cmp #2
      bne :+ ; octave 4
        lda #<piano_octave_4
        sta fixup_lookupnote+1
        lda #>piano_octave_4
        sta fixup_lookupnote+2
        rts
      :
      cmp #3
      bne :+ ; octave 5
        lda #<piano_octave_5
        sta fixup_lookupnote+1
        lda #>piano_octave_5
        sta fixup_lookupnote+2
        rts
      :
      cmp #4
      bne :+ ; octave 6
        lda #<piano_octave_6
        sta fixup_lookupnote+1
        lda #>piano_octave_6
        sta fixup_lookupnote+2
        rts
      :
      cmp #5
      bne :+ ; octave 7
        lda #<piano_octave_7
        sta fixup_lookupnote+1
        lda #>piano_octave_7
        sta fixup_lookupnote+2
        rts
      :
      cmp #6
      bne :+ ; octave 8
        lda #<piano_octave_8
        sta fixup_lookupnote+1
        lda #>piano_octave_8
        sta fixup_lookupnote+2
        rts
      :

      rts

  exit:
    clc
    rts

.endproc

DRAW_UI_PIANO:
    ; draw piano keyboard
    lda #3
    ldx #18
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL

    ROM_PRINT_CINV #txt_ui_piano1

    lda #3
    ldx #20
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL

    ROM_PRINT_CINV #txt_ui_piano2

    ; draw first line of help text
    lda #0
    ldx #12
    jsr SET_CURSOR

    ROM_PRINT_CINV #txt_ui_piano_help

    lda ui_piano_insert_mode
    beq :+
      ROM_PRINT_CINV #txt_ui_piano_help_write
    :

    ; draw second life of help text
    lda ui_piano_insert_mode
    beq :+
      lda #0
      ldx #14
      jsr SET_CURSOR

      ROM_PRINT_CINV #txt_ui_piano_help2
    :

    rts

DRAW_UI_PIANO_EXTRA:
    lda #25
    ldx #17
    jsr SET_CURSOR

    ROM_PRINT_CINV #txt_ui_piano_v
    lda ui_piano_volume
    jsr ROM::ROM_PRBYTE

    lda #25
    ldx #19
    jsr SET_CURSOR

    ROM_PRINT_CINV #txt_ui_piano_d
    lda ui_piano_duration
    jsr ROM::ROM_PRBYTE

    lda #25
    ldx #21
    jsr SET_CURSOR

    ROM_PRINT_CINV #txt_ui_piano_o
    lda ui_piano_octave
    clc
    adc #2
    jsr ROM::ROM_PRBYTE

    rts

SUB PIANO_INSERT_DELAY
    lda pattern_cursor
    cmp #$80
    bcc :+
      jsr PLAY_ERROR
      rts
    :
    jsr CLEAR_PATTERN_CURSOR
    
    lda pattern_cursor
    jsr INSERT_BLANK_PATTERN_NOTE

    lda #cmd_d
    ldx ui_piano_duration
    jsr MODIFY_CURR_PATTERN_NOTE

    jsr INC_PATTERN_CURSOR

    jsr CALC_PATTERN_CURSOR
    jsr DRAW_PATTERN_CURSOR
    jsr DRAW_PATTERN_EXTRA
    jsr DRAW_PATTERN
    rts
END_SUB

.export INSERT_PIANO_NOTE

.proc INSERT_PIANO_NOTE
    lastvol = temp

    lda pattern_cursor
    cmp #$80
    bcc :+
      jsr PLAY_ERROR
      rts
    :

    lda ui_piano_volume
    cmp lastvol
    beq :+
      ; volume change, must add new command
      lda pattern_cursor
      jsr INSERT_BLANK_PATTERN_NOTE

      lda #cmd_v
      ldx ui_piano_volume
      jsr MODIFY_CURR_PATTERN_NOTE

      jsr INC_PATTERN_CURSOR

      lda ui_piano_volume
      sta lastvol
    :

    ; now add the note
    lda pattern_cursor
    jsr INSERT_BLANK_PATTERN_NOTE

    lda ui_piano_freq
    ldx ui_piano_duration
    jsr MODIFY_CURR_PATTERN_NOTE

  exit:
    jsr INC_PATTERN_CURSOR
    rts
.endproc

SUB INSERT_PIANO_MODE
    lastvol = temp
    lastdur = temp+1

    lda #1
    sta ui_piano_insert_mode

    jsr PATTERN_FIND_LAST_CURSOR_INFO
    sta lastvol ; last volume
    stx lastdur ; last duration

    ; find from song slot
    lda lastvol
    sta ui_piano_volume

    ; find from song slot
    lda lastdur
    sta ui_piano_duration

    ; start out at octave 3 by default
    lda #2
    sta ui_piano_octave
    jsr STARTUP_PIANO

    ; copy pattern
    jsr BACKUP_PATTERN

    lda pattern_cursor
    sta ui_piano_original_cursor

  @loop:
    jsr PATTERN_FIND_LAST_CURSOR_INFO
    sta lastvol ; last volume
    stx lastdur ; last duration

    jsr DO_PIANO
    bcc :+
      jmp @exit
    :
    jsr CLEAR_PATTERN_CURSOR

    ; use piano_xxx to fill in note
    jsr INSERT_PIANO_NOTE

    lda pattern_cursor
    sta ui_piano_original_cursor

    jsr CALC_PATTERN_CURSOR
    jsr DRAW_PATTERN_CURSOR
    jsr DRAW_PATTERN_EXTRA

    jsr DRAW_PATTERN

    jmp @loop

  @exit:
    jsr SHUTDOWN_PIANO
    rts

END_SUB


.segment "DATA_H"

DEFDATA ui_piano_insert_mode
  .byte 1
END_DEFDATA

DEFDATA ui_piano_volume
  .byte $5
END_DEFDATA

DEFDATA ui_piano_duration
  .byte du_2
END_DEFDATA

DEFDATA ui_piano_freq
  .byte $00
END_DEFDATA

DEFDATA ui_piano_soft
  .byte $00
END_DEFDATA

DEFDATA ui_piano_octave
  .byte 1  ; 0=2, 1=3, 2=4, 3=5, 4=6, 5=7, 6=8
END_DEFDATA

DEFDATA ui_piano_original_cursor
  .byte $00
END_DEFDATA

.segment "RODATA_H"

.export piano_matrix, piano_octave_2, piano_octave_3, piano_octave_4, piano_octave_5, piano_octave_6, piano_octave_7, piano_octave_8
.export txt_ui_piano1, txt_ui_piano2, txt_ui_piano_d, txt_ui_piano_o, txt_ui_piano_v

txt_ui_piano1:
  .asciiz "   W E   T Y U"

txt_ui_piano2:
  .asciiz "  A S D F G H J"

txt_ui_piano_v:
  INVBYTE '-'
  .byte "/"
  INVBYTE '+'
  .asciiz " VOL="

txt_ui_piano_d:
  INVBYTE '['
  .byte "/"
  INVBYTE ']'
  .asciiz " DUR="

txt_ui_piano_o:
  INVBYTE 'U'
  .byte "/"
  INVBYTE 'D'
  .asciiz " OCT="

DEFDATA txt_ui_piano_help
  INVBYTE 'E'
  INVBYTE 'S'
  INVBYTE 'C'
  .byte "=EXIT "
  INVBYTE '<'
  .byte "/"
  INVBYTE '>'
  .byte "=CURSOR "
  INVBYTE 'P'
  .byte "LAY "
  .byte 0
END_DEFDATA

DEFDATA txt_ui_piano_help_write
  INVBYTE '1'
  .byte "-"
  INVBYTE '5'
  .byte "=DUR# "
  .byte 0
END_DEFDATA

DEFDATA txt_ui_piano_help2
  INVBYTE 'K'
  .byte "ILL-NOTE U"
  INVBYTE 'N'
  .byte "DO "
  .byte "DE"
  INVBYTE 'L'
  .byte "AY "
  .byte 0
END_DEFDATA

piano_matrix:
    .byte ROM::KB::kb_A, $00   ; C
    .byte ROM::KB::kb_W, $01   ; C#
    .byte ROM::KB::kb_S, $02   ; D
    .byte ROM::KB::kb_E, $03   ; D#
    .byte ROM::KB::kb_D, $04   ; E
    .byte ROM::KB::kb_F, $05   ; F
    .byte ROM::KB::kb_T, $06   ; F#
    .byte ROM::KB::kb_G, $07   ; G
    .byte ROM::KB::kb_Y, $08   ; G#
    .byte ROM::KB::kb_H, $09   ; A
    .byte ROM::KB::kb_U, $0A   ; A#
    .byte ROM::KB::kb_J, $0B   ; B

piano_octave_2:
    .byte no_2_C
    .byte no_2_Cs
    .byte no_2_D
    .byte no_2_Ds
    .byte no_2_E
    .byte no_2_F
    .byte no_2_Fs
    .byte no_2_G
    .byte no_2_Gs
    .byte no_2_A
    .byte no_2_As
    .byte no_2_B

piano_octave_3:
    .byte no_3_C
    .byte no_3_Cs
    .byte no_3_D
    .byte no_3_Ds
    .byte no_3_E
    .byte no_3_F
    .byte no_3_Fs
    .byte no_3_G
    .byte no_3_Gs
    .byte no_3_A
    .byte no_3_As
    .byte no_3_B

piano_octave_4:
    .byte no_4_C
    .byte no_4_Cs
    .byte no_4_D
    .byte no_4_Ds
    .byte no_4_E
    .byte no_4_F
    .byte no_4_Fs
    .byte no_4_G
    .byte no_4_Gs
    .byte no_4_A
    .byte no_4_As
    .byte no_4_B

piano_octave_5:
    .byte no_5_C
    .byte no_5_Cs
    .byte no_5_D
    .byte no_5_Ds
    .byte no_5_E
    .byte no_5_F
    .byte no_5_Fs
    .byte no_5_G
    .byte no_5_Gs
    .byte no_5_A
    .byte no_5_As
    .byte no_5_B

piano_octave_6:
    .byte no_6_C
    .byte 0
    .byte no_6_D
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte no_6_G
    .byte 0
    .byte 0
    .byte 0
    .byte 0

piano_octave_7:
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte no_7_F
    .byte 0
    .byte no_7_G
    .byte 0
    .byte no_7_A
    .byte 0
    .byte 0

piano_octave_8:
    .byte no_8_C
    .byte 0
    .byte 0
    .byte no_8_Ds
    .byte 0
    .byte 0
    .byte 0
    .byte no_8_G
    .byte 0
    .byte 0
    .byte 0
    .byte 0
