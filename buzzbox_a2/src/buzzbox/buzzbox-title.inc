.segment "CODE_H"

SUB DO_TITLE
		JSR ROM::TEXT_MODE_40
		JSR ROM::ROM_HOME ; home, clrscreen

		jsr ROM::DETECT_APPLE_TYPE
		jsr ROM::DETECT_APPLE_MEMORY

    lda #5
    ldx #5
    jsr SET_CURSOR

    ROM_PRINT #txt_title_logo1

    lda #13
    ldx #7
    jsr SET_CURSOR

    ROM_PRINT #txt_title_logo2


    lda #11
    ldx #9
    jsr SET_CURSOR

    ROM_PRINT #txt_title_logo3

    Util_LOAD #table_machtype, temp1w
    Util_LOAD #table_machmem, temp2w

    lda #1
    ldx #14
    jsr SET_CURSOR

    ROM_PRINT #txt_title_machtype

		lda ROM::machine_type
    asl
    sta temp1
    Util_Inc_16_Addr temp1w, temp1

    ldy #0
    lda (temp1w), y
    sta temp3w
    iny
    lda (temp1w), y
    sta temp3w+1

    ROM_PRINT temp3w

    lda #26
    ldx #14
    jsr SET_CURSOR

    ROM_PRINT #txt_title_machmem

		lda ROM::machine_ram
    asl
    sta temp1
    Util_Inc_16_Addr temp2w, temp1

    ldy #0
    lda (temp2w), y
    sta temp3w
    iny
    lda (temp2w), y
    sta temp3w+1

    ROM_PRINT temp3w
    
    lda #14
    ldx #19
    jsr SET_CURSOR

    ROM_PRINT #txt_title_press

    jsr ROM::ROM_GETKEY


    rts
END_SUB

.segment "RODATA_H"


txt_title_machtype:
  .asciiz "MACHINE: "

txt_title_machmem:
  .asciiz " MEMORY: "

txt_title_logo1:
  .asciiz "B U Z Z B O X    T R A C K E R"

txt_title_logo2:
  .asciiz "by David Grace"

txt_title_logo3:
  .asciiz "chainedlupine.com"

txt_title_press:
  .asciiz "PRESS A KEY"

table_machtype:
  .addr txt_mach_unknown
  .addr txt_mach_IIorig
  .addr txt_mach_IIplus
  .addr txt_mach_IIe
  .addr txt_mach_IIeEnh
  .addr txt_mach_IIc
  .addr txt_mach_III

table_machmem:
  .addr txt_mem_48
  .addr txt_mem_64
  .addr txt_mem_128

txt_mem_48:
  .asciiz "48k"

txt_mem_64:
  .asciiz "64k"

txt_mem_128:
  .asciiz "128k"

txt_mach_unknown:
  .asciiz "???"

txt_mach_IIorig:
  .asciiz "]["

txt_mach_IIplus:
  .asciiz "][+"

txt_mach_IIe:
  .asciiz "//e"

txt_mach_IIeEnh:
  .asciiz "//e Enh"

txt_mach_IIc:
  .asciiz "//c"

txt_mach_III:
  .asciiz "///"

