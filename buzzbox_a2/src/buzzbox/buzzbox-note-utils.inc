.segment "CODE_H"


SUB BACKUP_PATTERN
    srcptr    = temp1w
    dstptr    = temp2w

    Util_LOAD currpatternptr, srcptr
    Util_LOAD #backup_pattern, dstptr

    ldy #0
  loop:
    lda (srcptr), y
    sta (dstptr), y
   
    iny
    ; loop if wraps
    bne loop

    rts

END_SUB

SUB RESTORE_PATTERN
    srcptr    = temp1w
    dstptr    = temp2w

    Util_LOAD #backup_pattern, srcptr
    Util_LOAD currpatternptr, dstptr

    ldy #0
  loop:
    lda (srcptr), y
    sta (dstptr), y

    iny
    ; loop if wraps
    bne loop

    rts

END_SUB

.export INSERT_BLANK_PATTERN_NOTE

; a = position to add at
.proc INSERT_BLANK_PATTERN_NOTE
    insertpos   = temp1
    savefirst   = temp2
    savesecond  = temp3

    sta insertpos

    ; check to see if we're at the end of the song
    lda pattern_cursor
    cmp #$7F
    beq areequal

    ; start at end of song.
    ; at index, save index-2 data
    ; copy to index data
    ; continue to index-=2

    Util_LOAD currpatternptr, temp1w

    ldx #$7F
  copyloop:
    txa
    U_DEC    
    asl ; 2x
    tay

    lda (temp1w), y
    sta savefirst
    iny
    lda (temp1w), y
    sta savesecond

    txa
    asl ; 2x
    tay

    lda savefirst
    sta (temp1w), y
    iny
    lda savesecond
    sta (temp1w), y

    dex
    ; check to see if we're now at insertion point
    cpx insertpos
    beq areequal
    bcs copyloop

  areequal:
    ; we've copied notes upwards, now clear insertpos
    lda insertpos
    asl ; 2x
    tay

    lda #0
    sta (temp1w), y
    iny
    sta (temp1w), y

    rts

.endproc

.export MODIFY_CURR_PATTERN_NOTE

; cursor will be current index
; a = new freq/command
; x = new duration/data
.proc MODIFY_CURR_PATTERN_NOTE
  sta temp1
  stx temp2

  Util_LOAD currpatternptr, temp1w

  lda pattern_cursor
  asl ;2x
  tay

  lda temp1
  sta (temp1w), y
  iny
  lda temp2
  sta (temp1w), y

  rts
.endproc

.export INC_PATTERN_CURSOR
.proc INC_PATTERN_CURSOR
    inc pattern_cursor
    cmp #$7F
    bcc :+
      lda #0
      sta pattern_cursor
    :
    jsr CALC_PATTERN_CURSOR
    rts
.endproc

; uses cursor to find the last volume and duration
; return volume in A, duration in X
SUB PATTERN_FIND_LAST_CURSOR_INFO

    lastvol     = temp1
    lastdur     = temp2

    notecmd     = temp4
    durdata     = temp5

    lda #5
    sta lastvol
    lda #du_3
    sta lastdur

    Util_LOAD currpatternptr, temp1w

    ldx #0
  loop:
    txa
    asl ;2x
    tay

    lda (temp1w),y
    sta notecmd
    iny
    lda (temp1w),y
    sta durdata

    ; decode
    lda notecmd
    cmp #$F0
    bcs :+
      jmp @nocommand
    :
    ; we are a command
    sec
    sbc #$F0

    cmp #2 ; volume
    bne :+
      lda durdata
      sta lastvol      
    :
    jmp @next

  @nocommand:
    ; is a note
    lda temp5
    and #$7F
    sta lastdur

  @next:
    cpx pattern_cursor
    beq @exit

    inx
    cpx #$7F
    bcc loop

  @exit:
    lda lastdur
    cmp #$5
    bcs :+
      lda #$5
      sta lastdur
    :
    lda lastvol
    cmp #1
    bne :+
      lda #1
      sta lastvol
    :

    lda lastvol
    ldx lastdur

    rts
END_SUB

.export DELETE_PATTERN_NOTE

; a = position to delete at
.proc DELETE_PATTERN_NOTE
    delpos      = temp1
    savefirst   = temp2
    savesecond  = temp3

    sta delpos

    ; start at end of song.
    ; at index, save index-2 data
    ; copy to index data
    ; continue to index-=2

    Util_LOAD currpatternptr, temp1w

    lda delpos
    tax

  copyloop:
    txa
    U_INC ; next entry
    ; otherwise...
    asl ; 2x
    tay

    lda (temp1w), y
    sta savefirst
    iny
    lda (temp1w), y
    sta savesecond

    txa
    asl ; 2x
    tay

    lda savefirst
    sta (temp1w), y
    iny
    lda savesecond
    sta (temp1w), y

    inx
    cpx #$7F
    bcc copyloop

  done:
    ; clear last entry
    txa
    asl ;  2xa
    tay
    lda #0
    sta (temp1w), y
    iny
    sta (temp1w), y
  
    rts


.endproc

SUB PLAY_PATTERN_WITH_CALLBACK
    sta $c010 ; clear keyboard strobe
    Util_LOAD currpatternptr, arg1w
    Util_LOAD #MUSIC_KEYBOARD_ABORT_CALLBACK, arg2w
    lda #20
    sta arg3 ; just arg3 for a skip count so we're not doing a keyboard poll every tick (slows down music)
    sta ROM::SB_KEYB
    jsr SOUND_PLAY_SONG

    rts
END_SUB

SUB MUSIC_KEYBOARD_ABORT_CALLBACK
    inc arg3
    cmp #30
    bcs :+
      clc
      rts
    :
		lda ROM::IN_KEYB
		bmi :+
      clc
      rts
    :
    sta ROM::SB_KEYB
    sec
    rts
END_SUB

.export CLEAR_CURRENT_PATTERN

.proc CLEAR_CURRENT_PATTERN
    Util_LOAD currpatternptr, temp1w

    ldx #0
  clearloop:
    txa
    asl ; 2x
    tay

    lda #0
    sta (temp1w), y
    iny
    sta (temp1w), y

    inx
    cpx #$80
    bcc clearloop

    rts

.endproc

SUB CLEAR_ALL_SONG_DATA
    jsr CLEAR_ALL_PATTERNS
    jsr CLEAR_ALL_SEQUENCES
    jsr CLEAR_PATTERN_NAMES

    ldx #0
  clearname:
    lda #$00
    sta songname, x
    inx
    cpx #8
    bcc clearname

    ldx #0
  loopnamec:
    lda txt_default_song_name, x
    beq done
    sta songname, x
    inx
    cpx #8
    bcc loopnamec
  done:

  rts
END_SUB

SUB CLEAR_ALL_SEQUENCES
    ldx #0
  loop:
    lda #$FF
    sta seqstorage, x
    inx
    bne loop

    rts
END_SUB


SUB CLEAR_PATTERN_NAMES
    Util_LOAD #patternnames, temp1w
    Util_LOAD #txt_default_pattern_name, temp2w

    ldx #max_patterns
  looppatt:

    ldy #0
    loopnameb:
      lda #0
      sta (temp1w), y
      iny
      cpy #8
      bcc loopnameb


    ldy #0
    loopnamec:
      lda (temp2w), y
      beq namecopied
      sta (temp1w), y
      iny
      cpy #8
      bcc loopnamec

  namecopied:
    Util_Inc_16_Addr temp1w, #8

    dex
    bne looppatt
    
  done:
    rts
END_SUB

CLEAR_ALL_PATTERNS:
    ; clear all patterns
    Util_LOAD #patternstorage, temp1w

    ldx #max_patterns
  @ptrnclr:

      ldy #0
    @clrloop:
      lda #0
      sta (temp1w), y
      iny
      bne @clrloop

    ; move 256 bytes to next pattern
    Util_Inc_16_Addr temp1w, #$FF

    dex
    bne @ptrnclr

    rts

; a = pattern index
; return: a,x = address (lo, hi)
SUB CALCULATE_PATTERN_PTR
    pha ; save a
    ZP_SAVE    
    Util_LOAD #patternstorage, temp1w

    pla ; restore a

    clc
    adc temp1w+1
    tax
    U_PHX
    lda temp1w
    pha

    ZP_RESTORE
    pla     ; lo of addr
    U_PLX   ; hi of addr

    rts
END_SUB

; a = pattern index
; return: a,x = address (lo, hi)
SUB CALCULATE_PATTERNNAME_PTR
  ZP_SAVE

  asl
  asl
  asl
  sta temp1

  Util_LOAD #patternnames, temp1w
  Util_Inc_16_Addr temp1w, temp1
  lda temp1w
  ldx temp1w+1

  ZP_RESTORE
  rts
END_SUB

SUB SET_PATTERN_PTR
    lda currpattern
    jsr CALCULATE_PATTERN_PTR
    sta currpatternptr
    stx currpatternptr+1

    lda currpattern
    jsr CALCULATE_PATTERNNAME_PTR
    sta currpatternnameptr
    stx currpatternnameptr+1
    rts
END_SUB

SUB DUPLICATE_EXAMPLE_PATTERN

    srcptr    = temp4w
    dstptr    = temp5w

    Util_LOAD #examplesong, srcptr

    jsr SET_PATTERN_PTR
    Util_LOAD currpatternptr, dstptr

    ; copy songdata into patternstorage as #0
    ldy #0
  @cloop:
    lda (srcptr), y
    sta (dstptr), y
    beq @cstop

    iny
    lda (srcptr), y
    sta (dstptr), y
    iny
    jmp @cloop

  @cstop:
    rts
END_SUB

SUB PLAY_ALL_PATTERN
    jsr CLEAR_UI_PATTERN_KEYS

    jsr PLAY_PATTERN_WITH_CALLBACK

    jsr DRAW_UI_PATTERN_KEYS
END_SUB


SUB CALCULATE_SEQS_USED
    ldx #0
  loop:
    lda seqstorage, x
    beq done
    inx
    cpx #$80
    bcc loop
  done:
    txa
    rts
END_SUB

SUB CALCULATE_PATTERNS_USED
    jsr CALCULATE_PATTERN_LENS
    ldx #0
    ldy #0
  loop:
    lda patternslen, x
    beq skip
    iny
  skip:
    inx
    cmp #$80
    bcc loop

    tya
    rts
END_SUB

; rebuild header
SUB CALCULATE_SONG_HEADER
    ; clear header
    ldx #songheader_end - songheader - 1
    lda #0
  loop:
    sta songheader, x
    dex
    bpl loop

    lda #$42
    sta songheader
    lda #$54
    sta songheader+1
    jsr CALCULATE_SEQS_USED
    sta songheader+2 ; song len
    jsr CALCULATE_PATTERNS_USED
    sta songheader+3 ; patterns used 

    lda #$00
    sta songheader+4 ; version 0

  rts
END_SUB

SUB VERIFY_SONG_HEADER
    lda songheader
    cmp #$42
    bne error

    lda songheader+1
    cmp #$54
    bne error

    lda songheader+2
    cmp #128 ; verify song len is <128
    bcs error

    lda songheader+3
    cmp #32 ; verify pattern len <32
    bcs error

    ; rest we don't care about
    clc
    rts

  error:  
    sec
    rts
END_SUB

; a = spot to delete at
SUB DELETE_SEQ_AT
    tax
  loop:
    inx
    lda seqstorage, x
    dex
    sta seqstorage, x
    inx
    cpx #$7F
    bne loop

    ; last place, delete
    lda #$FF
    sta seqstorage, x
    rts
END_SUB

; a = spot to insert blank at
SUB INSERT_BLANK_SEQ_AT
    sta temp5
    ldx #$7F
  loop:
    dex
    lda seqstorage, x
    inx
    sta seqstorage, x

    dex
    beq done
    cpx temp5
    bne loop

  done:

    ; last place, delete
    lda #$FF
    sta seqstorage, x
    rts

END_SUB


SUB CALCULATE_SEQ_PTRS
    currpatt = temp1

    ; first fill with $FFFF
    ldx #0
  floop:
    lda #$FF
    sta seqptrstorage, x

    inx
    bne floop

    ldy #0
  loop:
    tya
    tax
    lda seqstorage, x
    cmp #$FF
    beq done

    sta currpatt

    Util_LOAD #patternstorage, temp1w
    lda temp1w+1
    clc
    adc currpatt
    sta temp1w+1

    tya
    asl ; 2x
    tax
    lda temp1w
    sta seqptrstorage, x
    inx
    lda temp1w+1
    sta seqptrstorage, x

  next:
    iny
    cmp #$80
    bcc loop

  done:
    rts
END_SUB

.macro INVBYTE arg
  .byte arg + $80
.endmacro
