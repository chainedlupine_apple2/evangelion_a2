.include "apple2rom.inc"
.include "prodos.inc"
.include "utils.inc"
.include "file.inc"
.include "zeropage.inc"
.include "auxtrampoline.inc"
.include "grfx.inc"
.include "kr6502decoder.inc"
.include "textengine.inc"

.segment "CODE_END_H"

		IMG_BUFFER_READ_AMOUNT 	= $2000
		
		; this a temporary buffer is in AUX
		dekompress_buffer_aux	 := $7b00  ; to 0x9aff

		; arg1w = addr to filename path
		; return = no carry if successful
FILE_OPEN:
		; ------- attempt to load a file into some buffers -----
		Util_LOAD arg1w, open_param+ProDOS::STRUCT_OPEN_PARAM::PATHNAME
		
		ProDOS_OPEN open_param
		rts

		; assumes a file is open already, will close when done
		; file REF_NUM should be in open_param already
		; arg2w = addr to input buffer (must be $2000 bytes in main)
FILE_LOAD_DHGR_TO_RAM:		
		; file is open, so now read
		LDA open_param+ProDOS::STRUCT_OPEN_PARAM::REF_NUM		
		STA read_param+ProDOS::STRUCT_READ_PARAM::REF_NUM
		STA close_param+ProDOS::STRUCT_CLOSE_PARAM::REF_NUM
		
		; -- handle AUX load -------------------------		
		Util_LOAD arg2w, read_param+ProDOS::STRUCT_READ_PARAM::DATA_BUFFER
				
		LDA #<IMG_BUFFER_READ_AMOUNT
		STA read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT
		LDA #>IMG_BUFFER_READ_AMOUNT
		STA read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT+1

		ProDOS_READ read_param
		bcc     :+
    jmp     EXIT_PRODOSERROR
		
		; now compare data read
:		LDA #<IMG_BUFFER_READ_AMOUNT
		CMP read_param+ProDOS::STRUCT_READ_PARAM::TRANS_COUNT
		BEQ :+
		LDA #$ff
		JMP EXIT_PRODOSERROR
:		LDA #>IMG_BUFFER_READ_AMOUNT
		CMP read_param+ProDOS::STRUCT_READ_PARAM::TRANS_COUNT+1
		BEQ :+
		LDA #$ff
		JMP EXIT_PRODOSERROR
:		; all good

		; now copy to AUX (in same location)
		; we're taking the data we loaded into the buffer in main (at arg2w) and we're moving it into a buffer
		MAIN2AUX_MEMCOPY arg2w, arg2w, #$2000

		; done with AUX
		
		; -- handle MAIN load -------------------------		
		Util_LOAD arg2w, read_param+ProDOS::STRUCT_READ_PARAM::DATA_BUFFER
				
		LDA #<IMG_BUFFER_READ_AMOUNT
		STA read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT
		LDA #>IMG_BUFFER_READ_AMOUNT
		STA read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT+1

		ProDOS_READ read_param		
		bcc     :+
    jmp     EXIT_PRODOSERROR

		; now compare data read
:		LDA #<IMG_BUFFER_READ_AMOUNT
		CMP read_param+ProDOS::STRUCT_READ_PARAM::TRANS_COUNT
		BEQ :+
		LDA #$ff
		JMP EXIT_PRODOSERROR
:		LDA #>IMG_BUFFER_READ_AMOUNT
		CMP read_param+ProDOS::STRUCT_READ_PARAM::TRANS_COUNT+1
		BEQ :+
		LDA #$ff
		JMP EXIT_PRODOSERROR
:		; all good

		; done with MAIN
		
		; -- closing file ---------------------------
		ProDOS_CLOSE close_param
		bcc     :+
  	jmp     EXIT_PRODOSERROR
:		
		; -- all done -------------------------------
		RTS

	skip_to_left_bpe = 2 + 2 + 2
	skip_to_right_bpe = (2 + 2 + 2 + 128)
	skip_to_encoded = $0106  ;(2 + 2 + 2 + 256)

		; assumes a file is open already, will close when done
		; file REF_NUM should be in open_param already
		; will load the entire compressed data to imgbuffer, and then it will decompress it
FILE_LOAD_KOMPRESSED_DHGR_TO_RAM:		
		; file is open, so now read
		LDA open_param+ProDOS::STRUCT_OPEN_PARAM::REF_NUM		
		STA read_param+ProDOS::STRUCT_READ_PARAM::REF_NUM
		STA close_param+ProDOS::STRUCT_CLOSE_PARAM::REF_NUM
		
		; -- load all kompressed data -------------------------		
		Util_LOAD #imgbuffer, read_param+ProDOS::STRUCT_READ_PARAM::DATA_BUFFER
				
		LDA #<IMG_BUFFER_READ_AMOUNT
		STA read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT
		LDA #>IMG_BUFFER_READ_AMOUNT
		STA read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT+1

		ProDOS_READ read_param
		bcc     :+
    jmp     EXIT_PRODOSERROR
	:
		Util_LOAD #imgbuffer, arg2w

		; now that the compressed data is in arg2w, we need to decompress the two chunks (AUX, then MAIN)
		; Steps are:
		; 1. Decompress from imgbuffer (MAIN) the AUX chunk straight to imgbuffer (AUX)
		; 2. Decompress from imgbuffer (MAIN) the MAIN chunk to dekompress_buffer_aux
		; 3. Copy dekompress_buffer_aux to imgbuffer (MAIN)
		
		; do some sanity checks first, like verify magic numbers
		ldy #0
		lda (arg2w), y
		cmp #$44
		beq :+
		lda #$F0
		jmp EXIT_DECOMPRESSOR_ERROR
	:
		ldy #1
		lda (arg2w), y
		cmp #$52
		beq :+
		lda #$F1
		jmp EXIT_DECOMPRESSOR_ERROR
  :

		; ------ AUX buffer ------------------------------------------

		; so, imgbuffer ($9b00) now holds the compressed file, which is <8192 bytes so it will fit
		; we will then decompress the first chunk in this file (offset $106 bytes) into imgbuffer in AUX
		
		; alright, magic bytes has been verified, now let's get the size of the first chunk
		ldy #2
		lda (arg2w), y
		sta arg3w
		ldy #3
		lda (arg2w), y
		sta arg3w+1
		
		; arg1w (dst) is to now point to imgbuffer (will be put into AUX)
		Util_LOAD #imgbuffer_aux, arg1w

		; load the addresses to the left BPE tables into arg4w
		Util_LOAD arg2w, arg4w
		Util_Inc_16_Addr arg4w, #skip_to_left_bpe

		; load the addresses to the right BPE tables into arg5w
		Util_LOAD arg2w, arg5w
		Util_Inc_16_Addr arg5w, #skip_to_right_bpe

		;jmp @skip2

		; increment arg2w to actual compressed data
		Util_Inc_16_Addr_16_Lit arg2w, skip_to_encoded

		; kecompress first chunk
		JSR KR6502_DECODE_MAIN_TO_AUX

		; ------ MAIN buffer ------------------------------------------

		; imgbuffer ($9b00) still holds the compressed file
		; we will then decompress the second chunk in this file (offset $89d bytes) into dekompress_buffer_aux ($8000) in AUX
		; once that is done, we will copy dekompress_buffer_aux into imgbuffer in MAIN

		; now we will dekompress the main image buffer
		Util_LOAD #imgbuffer, arg2w
		; arg1w (dst) is to now point to dekompress_buffer_aux (temp storage in aux)
		Util_LOAD #dekompress_buffer_aux, arg1w

		; load the addresses to the left BPE tables into arg4w
		Util_LOAD arg2w, arg4w
		Util_Inc_16_Addr arg4w, #skip_to_left_bpe

		; load the addresses to the right BPE tables into arg5w
		Util_LOAD arg2w, arg5w
		Util_Inc_16_Addr arg5w, #skip_to_right_bpe

		ldy #2
		lda (arg2w), y
		sta temp1
		ldy #3
		lda (arg2w), y
		sta temp2

		; get size of second chunk
		ldy #4
		lda (arg2w), y
		sta arg3w
		ldy #5
		lda (arg2w), y
		sta arg3w+1

		; increment arg2w to actual compressed data
		Util_Inc_16_Addr_16_Lit arg2w, skip_to_encoded
		Util_Inc_16_Addr_16_Addr arg2w, temp1	; advance the encoded data to the start of the second chunk

		; kecompress second chunk
		JSR KR6502_DECODE_MAIN_TO_AUX

		; now copy it over the MAIN imgbuffer
		AUX2MAIN_MEMCOPY #imgbuffer, #dekompress_buffer_aux, #IMG_BUFFER_READ_AMOUNT

		; we are done with both banks

		; -- closing file ---------------------------
		ProDOS_CLOSE close_param
		bcc     :+
  	jmp     EXIT_PRODOSERROR
:		
		; -- all done -------------------------------
		RTS



		; assumes a file is open already, will close when done
		; file REF_NUM should be in open_param already
		; will load the entire compressed data to te_kompressed_text
FILE_LOAD_KOMPRESSED_TEXT_TO_RAM:		
		; file is open, so now read
		LDA open_param+ProDOS::STRUCT_OPEN_PARAM::REF_NUM		
		STA read_param+ProDOS::STRUCT_READ_PARAM::REF_NUM
		STA close_param+ProDOS::STRUCT_CLOSE_PARAM::REF_NUM
		
		; -- load all kompressed data -------------------------		
		Util_LOAD #te_kompressed_text, read_param+ProDOS::STRUCT_READ_PARAM::DATA_BUFFER
				
		LDA #<te_kompressed_maxsize
		STA read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT
		LDA #>te_kompressed_maxsize
		STA read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT+1

		ProDOS_READ read_param
		bcc     :+
    jmp     EXIT_PRODOSERROR
	:
		Util_LOAD #te_kompressed_text, arg2w

		; we have loaded the text data into te_kompressed_text in LO ram, that's all need to do, aside from...

		; do some sanity checks first, like verify magic numbers
		ldy #0
		lda (arg2w), y
		cmp #$44
		beq :+
		lda #$F0
		jmp EXIT_DECOMPRESSOR_ERROR
	:
		ldy #1
		lda (arg2w), y
		cmp #$54
		beq :+
		lda #$F1
		jmp EXIT_DECOMPRESSOR_ERROR
  :

		; -- closing file ---------------------------
		ProDOS_CLOSE close_param
		bcc     :+
  	jmp     EXIT_PRODOSERROR
:		
		; -- all done -------------------------------
		RTS


; A = ProDOS error code				
EXIT_PRODOSERROR:
		PHA
		JSR ROM::TEXT_MODE_40
		ROM_PRINT #error_file_msg
		PLA
		JSR ROM::ROM_PRBYTE
		JSR ROM::ROM_GETKEY ; GETKEY

		JMP EXIT_PRODOS

; A = error code
EXIT_DECOMPRESSOR_ERROR:
		PHA
		JSR ROM::TEXT_MODE_40
		ROM_PRINT #error_decompress_msg
		PLA
		JSR ROM::ROM_PRBYTE
		JSR ROM::ROM_GETKEY ; GETKEY

		JMP EXIT_PRODOS

; --------------------------
; some ProDOS entrance stuff
; --------------------------

EXIT_PRODOS:
		JSR ProDOS::RAMDISK_RECONNECT
		ProDOS_QUIT (quit_param)
		RTS
		
START_PRODOS:
		JSR ProDOS::RAMDISK_DISCONNECT
		RTS

FILE_SET_VOLUME1:
		;Util_LOAD #volname_disk1, getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
		ProDOS_SET_PREFIX getset_prefix_param
		rts

FILE_SET_VOLUME2:
		;Util_LOAD #volname_disk2, getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
		ProDOS_SET_PREFIX getset_prefix_param
		rts

		; forces all devices to rescan their ProDOS volumes
FILE_DISK_ALL_ON_LINE:
		; we don't care about results, just us io_buffer
		Util_LOAD #io_buffer, on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
		ProDOS_ON_LINE on_line_param
		rts

		; reloads the volume name found in the boot drive (usually slot 6, drive 1)
		; it then attempts to set our current prefix to that
FILE_RESYNC_VOLUME_NAME:
		; force just our boot device to rescan its ProDOS volume
		Util_LOAD #io_buffer, on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
		lda boot_unit_num
		cmp #$ff
		bne :+
		jmp @vol_name_error
	:
		sta on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::UNIT_NUM
		ProDOS_ON_LINE on_line_param
		
		; copy it over to io_buffer+512 bytes, turning it into a set_prefix style name in the process
		lda #$2F
		sta io_buffer+$201 ; first write $2F to io_buffer+$201

		; have to do this by adding one to the length and preceeding it with a / ($2F)
		lda io_buffer
		; mask off unit_num
		and #$0F
		beq @vol_name_error

		inc ; add one to length
		sta io_buffer+$200  ; store at beginning of buffer
		dec ; return to normal
		tax
	@loop_char:
		lda io_buffer,x
		sta io_buffer+$201,x
		dex
		bne @loop_char

		; now our new volume prefix should be at io_buffer+$200

		; now set our new prefix
		Util_LOAD #io_buffer+$200, getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
		ProDOS_SET_PREFIX getset_prefix_param

	@vol_name_error:
		; something wrong w/ volume, just return

		rts


FILE_GET_BOOT_UNIT_NUM:
		; store our current prefix in self half of io_buffer
		Util_LOAD #io_buffer+$200, getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
		ProDOS_GET_PREFIX getset_prefix_param

		; force all devices to rescan their ProDOS volumes
		Util_LOAD #io_buffer, on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
		Util_LOAD #$00, on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::UNIT_NUM
		ProDOS_ON_LINE on_line_param

		; now scan online records, looking for the prefix that we booted with
		Util_LOAD #io_buffer, temp1w
		Util_LOAD #io_buffer+$201, temp2w	; skip one bytes, because first byte of GET_PREFIX is len of string, and second is always / ($2F)

		; store error'd boot unit number for now		
		lda #$ff
		sta boot_unit_num

		lda #0
		sta temp5		; holds current record idx count
	@loop_record:
		lda (temp1w)
		tax ; save first byte for now
		and #$F0	; strip off the length
		bne :+
			; unit is zero, so we're at end
			jmp @done
		:
		sta temp3 ; store unit number temporarily for now
		txa ; restore so we can check disk/slot #
		and #$0F	; strip off the disk/slot
		bne :+
			; len=0, so this must be errored device, skip
			jmp @next_record
		:
		sta temp4 ; store length temporarily too
		tay	; store length in y
		; now, let's compare the prefix to the one we saved
	@loop_char:
		lda (temp1w),y
		sta temp6
		lda (temp2w),y
		cmp temp6
		bne @next_record  ; didn't match, so skip
		dey
		bne @loop_char
		; no mismatch found, so we must match
		
	@match_found:
		; write boot number from temp store
		lda temp3 
		sta boot_unit_num

		; now write volume name to boot_volname
		ldy temp4 ; write length to first byte
		iny ; increment by one, because we're adding / at beginning
		sty boot_volname

		lda #$2F
		sta boot_volname+1  ; write / ($2F) to first character


		; now copy over name
	@copy_char:
	
    lda (temp1w),y
		sta boot_volname+1,y
		dey
		bne @copy_char

		jmp @done

	@next_record:
			Util_Inc_16_Addr temp1w, #$10
			inc temp5  ; increase record index count
			lda temp5	 
			cmp #$0F	 ; check to see if count is >15, if so loop again
			bcc @loop_record

			; all records exhausted.
			; and no matches found...

	@done:
			rts


.segment "RODATA_H"

	error_file_msg: 			.asciiz "EXUBER& fatal ProDOS error: code = "
	error_decompress_msg: .asciiz "EXUBER& decompressor error: code = "

.segment "DATA_H"

	boot_unit_num:				.byte $00
	boot_volname:					.res $17, $00

	on_line_param:
		ProDOS_DEFINE_ON_LINE_PARAM

	getset_prefix_param:
		ProDOS_DEFINE_GETSET_PREFIX_PARAM

	open_param:
		ProDOS_DEFINE_OPEN_PARAM io_buffer
	
	quit_param:
		ProDOS_DEFINE_QUIT_PARAM 

	read_param:
		ProDOS_DEFINE_READ_PARAM
		
	close_param:
		ProDOS_DEFINE_CLOSE_PARAM

.segment "IOBUFFER_H"
		io_buffer: 	; 1024 bytes
			.res $400, $00