.include "apple2rom.inc"
.include "prodos.inc"
.include "apple2rom-defs.inc"
.include "../zeropage.inc"
.include "utils.inc"

.include "../file.inc"
.include "../buzzbox/sound.inc"
.include "sound-tester.inc"

.segment "CODE_H"

		; reset stack
		ldx     #$FF
		txs

    lda #0
    sta ROM::ROM_WNDTOP
		lda #22
		sta ROM::ROM_WNDBTM
		lda #0
		sta ROM::ROM_WNDLFT
		lda #40
		sta ROM::ROM_WNDWDTH
    sta ROM::ROM_VTAB


		JSR CHECK_FOR_BASIC

		ZP_SETUP

		; initial ProDOS SYSTEM setup
    JSR START_FROM_PRODOS

		JSR ROM::TEXT_MODE_40
		JSR ROM::ROM_HOME ; home, clrscreen

    JSR SOUND_TESTER

    JSR QUIT_TO_PRODOS

START_FROM_PRODOS:
		JSR ProDOS::RAMDISK_DISCONNECT
		RTS


QUIT_TO_PRODOS:
		JSR ProDOS::RAMDISK_RECONNECT
		ProDOS_QUIT (quit_param)

CHECK_FOR_BASIC:
		lda $33
		cmp #$dd
		bne :++
			lda $76
			cmp #$ff
			bne :+
				JSR ROM::TEXT_MODE_40
				ROM_PRINT #txt_basic

				JSR ROM::ROM_RDKEY

				JMP QUIT_TO_PRODOS
			:
		:
		rts
		
; -- data ------------------------------------------------------

.segment "RODATA_H"

txt_basic:				.asciiz "Please boot straight from ProDOS        and do not use Applesoft. :)"

quit_param:
		ProDOS_DEFINE_QUIT_PARAM 
