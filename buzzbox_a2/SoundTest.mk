# be careful, any BINFILE name must be 8 chars max
BUILDDST=build/sndtest

CURR_MAKEFILE	:= $(lastword $(MAKEFILE_LIST))

BINFILE = SNDTEST
BINFILE_PATH = $(BUILDDST)
BINFILE_FULL = $(BINFILE_PATH)/$(BINFILE).bin
OBJS = main.o sound.o zeropage.o
LIBS = apple2rom.o prodos.o
TARGET_ARCH = apple2enh
TARGET_MEMORY_MAP = memory-64k.cfg

# disk file to create to hold binfile
DISKIMAGE = $(BUILDDST)/SNDTEST.PO
DISKVOLUME = SNDTEST

ASSETS_DISK = 

OBJ_PATH = $(BUILDDST)/obj

CPU_TYPE = 6502

SOUND_INC = src/buzzbox/sound.inc $(INCLUDE_PATH)/utils.inc src/zeropage.inc src/buzzbox/sound-music.inc

include lupinecore_a2/binasm.mk
include lupinecore_a2/diskpack.mk
include lupinecore_a2/assettools.mk

$(BUILDDST)/obj/zeropage.o:	src/zeropage.s src/zeropage.inc
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/sound.o: src/buzzbox/sound.s $(SOUND_INC)
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/main.o: src/sndtest/main.s src/file.inc  $(SOUND_INC) src/sndtest/sound-tester.inc
	$(CA65) $(AFLAGS) $< -o $@

# kinda ugly, recursive makes but didn't want to use .phonys
$(DISKIMAGE): $(BINFILE_FULL) $(ASSETS_DISK1)
	$(MAKE) -f $(CURR_MAKEFILE) DISK_TO_MAKE=$(DISKIMAGE) DISK_VOLUME_TO_MAKE=$(DISKVOLUME) makeprodos 
	$(MAKE) -f $(CURR_MAKEFILE) DISK_TO_MAKE=$(DISKIMAGE) BIN_TO_LOAD=$(BINFILE) BIN_TO_LOAD_FULL=$(BINFILE_FULL) BIN_TO_LOAD_START=$(BINSTART) loadbin_selfstartprodos

.PHONY: all clean cleanassets assets dskHDD cleandsk test

test:
	-lupinecore_a2/extern/AppleWin/Applewin.exe -no-printscreen-key -d1 $(DISKIMAGE) -s7 empty

assets: $(ASSETS_DISK)

all: | bin dsk

dsk: $(DISKIMAGE)

cleandsk:
	-rm -f $(DISKIMAGE)

clean: cleanbin cleandsk
