# be careful, any BINFILE name must be 8 chars max
BUILDDST=build/buzzbox

CURR_PATH	:= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
CURR_MAKEFILE	:= $(lastword $(MAKEFILE_LIST))

BINFILE = BUZZBOX
BINFILE_PATH = $(BUILDDST)
BINFILE_FULL = $(BINFILE_PATH)/$(BINFILE).bin
OBJS = main.o sound.o zeropage.o buzzbox.o fileio.o
LIBS = apple2rom.o prodos.o
TARGET_ARCH = apple2enh
TARGET_MEMORY_MAP = memory-64k.cfg

# disk file to create to hold binfile
DISKIMAGE = $(BUILDDST)/BUZZBOX.PO
DISKVOLUME = BUZZBOX.DISK

ASSETS_DISK = 

OBJ_PATH = $(BUILDDST)/obj

PRODOS_BOOT_DISK = $(BUILDDST)/ProDOS_2_4_2.dsk

include lupinecore_a2/binasm.mk
include lupinecore_a2/diskpack.mk
include lupinecore_a2/assettools.mk

SOUND_INC = $(INCLUDE_PATH)/utils.inc src/zeropage.inc src/buzzbox/sound.inc src/buzzbox/sound-music.inc src/buzzbox/buzzbox.inc

COMMON_INCLUDE = $(INCLUDE_PATH)/utils.inc src/zeropage.inc

BUZZ_INC 	= src/buzzbox/buzzbox-pattern-editor.inc src/buzzbox/buzzbox-piano.inc src/buzzbox/buzzbox-ui-texted.inc 
BUZZ_INC += src/buzzbox/buzzbox-ui.inc src/buzzbox/buzzbox-note-utils.inc src/buzzbox/buzzbox-settings.inc
BUZZ_INC += src/buzzbox/buzzbox-rawmode.inc src/buzzbox/fileio.inc src/buzzbox/buzzbox-song-editor.inc
BUZZ_INC += src/buzzbox/buzzbox-ui-song.inc src/buzzbox/buzzbox-title.inc src/buzzbox/buzzbox-disk.inc

CPU_TYPE = 6502

$(BUILDDST)/obj/zeropage.o:	src/zeropage.s src/zeropage.inc
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/fileio.o: src/buzzbox/fileio.s src/buzzbox/fileio.inc $(COMMON_INCLUDE) 
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/sound.o: src/buzzbox/sound.s $(SOUND_INC)
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/buzzbox.o: src/buzzbox/buzzbox.s $(SOUND_INC) $(BUZZ_INC)
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/main.o: src/buzzbox/main.s src/file.inc $(SOUND_INC)
	$(CA65) $(AFLAGS) $< -o $@

# kinda ugly, recursive makes but didn't want to use .phonys
$(DISKIMAGE): $(BINFILE_FULL) $(ASSETS_DISK1) 
	$(MAKE) -f $(CURR_MAKEFILE) DISK_TO_MAKE=$(DISKIMAGE) DISK_VOLUME_TO_MAKE=$(DISKVOLUME) makeprodosbasic
	$(AC) -p $(DISKIMAGE) BUZZBOX.EDITOR sys 0x2000 <$(BUILDDST)/BUZZBOX.bin

mount: $(DISKIMAGE)
	-cp -f $(DISKIMAGE) ../../ADTPro-2.0.3/disks/BUZZBOX/Virtual.po

test:
	-lupinecore_a2/extern/AppleWin/Applewin.exe -no-printscreen-key -d1 $(CURR_PATH)/$(PRODOS_BOOT_DISK) -s7 empty

.PHONY: all clean cleanassets assets dskHDD cleandsk dsk test cleantxt mount

assets: $(ASSETS_DISK)

all: | bin dsk

dsk: $(DISKIMAGE)

cleanassets: $(ASSETS)
	-rm -f $(ASSETS_DISK)

cleandsk:
	-rm -f $(DISKIMAGE)

clean: cleanbin cleandsk
