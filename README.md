# EVANGELION A.2

This is Evangelion on an Apple II.

My contribution to the current (2019) Neon Genesis Evangelion memes that are
ongoing!

A small animated project I put together, mostly as a test of my Buzzbox music
library and Bootspot routines (along with the HGR mode of palettehgr).

This is a ProDOS program which runs on any Apple II Plus or higher with 64KB of
RAM.

Compiles with cc65 and uses AppleCommander to build Apple II disk images.

# Downloading

Current release is [HERE](https://gitlab.com/chainedlupine_apple2/evangelion_a2/blob/91583f44c1a1f54441ffb86ad4456dbbead15058/releases/3.00/EVA-A2-v3.00.zip) as a ProDOS self-booting disk image.

You can copy this directly to an Apple II 5.25 floppy via ADTPro.

# Compiling

To compile, you will need to set up the [LupineCore A2](https://gitlab.com/chainedlupine_apple2/lupinecore_a2) tool chain which is a subtree in lupinecore_a2.

For details, see that project's README.