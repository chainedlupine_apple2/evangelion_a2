# be careful, any BINFILE name must be 8 chars max
BUILDDST=build/nsr

CURR_PATH	:= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
CURR_MAKEFILE	:= $(lastword $(MAKEFILE_LIST))

BINFILE = FLYME
BINFILE_PATH = $(BUILDDST)
BINFILE_FULL = $(BINFILE_PATH)/$(BINFILE).bin
OBJS = main.o sound.o zeropage.o fileio.o nsr.o
LIBS = apple2rom.o prodos.o
TARGET_ARCH = apple2enh
TARGET_MEMORY_MAP = src/nakedspinningrei/memory-nsr.cfg

# disk file to create to hold binfile
DISKIMAGE = $(BUILDDST)/EVA.A2.PO
DISKVOLUME = EVA.A2.DISK

ASSETS = 
ASSETS_DISK = 

OBJ_PATH = $(BUILDDST)/obj
ASSET_BUILD_PATH = $(BUILDDST)/assets

include lupinecore_a2/binasm.mk
include lupinecore_a2/diskpack.mk
include lupinecore_a2/assettools.mk

SOUND_INC = $(INCLUDE_PATH)/utils.inc src/zeropage.inc buzzbox_a2/src/buzzbox/sound.inc buzzbox_a2/src/buzzbox/sound-music.inc

COMMON_INCLUDE = $(INCLUDE_PATH)/utils.inc src/zeropage.inc

MAIN_INCLUDE = src/nakedspinningrei/nsr.inc $(BUILDDST)/assets/flyme1.inc

CPU_TYPE = 6502

PRODOS_BOOT_DISK = $(BUILDDST)/ProDOS_2_4_2.dsk

ASSETS_INCLUDE = assets/nge-ending-rei.hgr assets/nge-ending-moon.hgr assets/startup-logo.hgr assets/gateopen.hgr

TEST_IMAGE_DISK = $(BUILDDST)/image-test-disk.po

ASM_STUB_BIN = $(BUILDDST)/ASMSTUB.bin

EVA_SYSTEM = $(BUILDDST)/EVA.SYSTEM

$(BUILDDST)/assets/flyme1.inc: $(BUILDDST)/assets/flyme1.bbt
	python3 buzzbox_a2/src/buzzbox/bbt-converter.py $(BUILDDST)/assets/flyme1.bbt

$(BUILDDST)/assets/flyme1.bbt: | $(ASSET_BUILD_PATH)
	$(AC) -g assets/music/music.po FLYME1.BBT > $(BUILDDST)/assets/flyme1.bbt

assets/nge-ending-moon.hgr: assets/nge-ending-moon.png
	$(PALETTEDHGR) --format=hgr --color=ntsc --png=$<

assets/nge-ending-rei.hgr: assets/nge-ending-rei.png
	$(PALETTEDHGR) --format=hgr --color=ntsc --png=$<

assets/startup-logo.hgr: assets/startup-logo.png
	$(PALETTEDHGR) --format=hgr --color=ntsc --png=$<

assets/gateopen.hgr: assets/gateopen.png
	$(PALETTEDHGR) --format=hgr --color=ntsc --png=$<

$(BUILDDST)/obj/zeropage.o:	src/zeropage.s src/zeropage.inc
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/fileio.o: buzzbox_a2/src/buzzbox/fileio.s buzzbox_a2/src/buzzbox/fileio.inc $(COMMON_INCLUDE) 
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/sound.o: buzzbox_a2/src/buzzbox/sound.s $(SOUND_INC)
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/nsr.o: src/nakedspinningrei/nsr.s $(SOUND_INC) $(COMMON_INCLUDE) $(MAIN_INCLUDE) $(ASSETS_INCLUDE)  src/nakedspinningrei/hgr-table.inc
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/main.o: src/nakedspinningrei/main-rei.s $(SOUND_INC)
	$(CA65) $(AFLAGS) $< -o $@

# kinda ugly, recursive makes but didn't want to use .phonys
$(DISKIMAGE): $(BINFILE_FULL) $(ASSETS_DISK1) $(ASM_STUB_BIN) $(EVA_SYSTEM)
	$(MAKE) -f $(CURR_MAKEFILE) DISK_TO_MAKE=$(DISKIMAGE) DISK_VOLUME_TO_MAKE=$(DISKVOLUME) makeprodosbasic
	$(AC) -p $(DISKIMAGE) STARTUP bas 0x801	<assets/basic/STARTUP#fc0801
	$(AC) -p $(DISKIMAGE) FLYME.DAT bin 0x6000 <$(BUILDDST)/FLYME.bin
	$(AC) -p $(DISKIMAGE) EVA.RUN sys 0x2000 <$(BUILDDST)/EVA.SYSTEM
	$(AC) -p $(DISKIMAGE) NERV.DAT bin 0x2000 <assets/startup-logo.hgr
	$(AC) -p $(DISKIMAGE) GATE.DAT bin 0x2000 <assets/gateopen.hgr
	$(AC) -p $(DISKIMAGE) ASMSTUB.DAT bin 0x300	<$(ASM_STUB_BIN)

$(ASSET_BUILD_PATH):
	$(call make_path, $@)

copydsk: $(DISKIMAGE)
	-cp -f $(DISKIMAGE) ../../ADTPro-2.0.3/disks/CL

#mount: $(DISKIMAGE)
#	-cp -f $(DISKIMAGE) ../../ADTPro-2.0.3/disks/BUZZBOX/Virtual.po

$(ASM_STUB_BIN):
	$(MAKE) -f NSR_AsmStub.mk bin

$(EVA_SYSTEM):
	$(MAKE) -f $(CURR_MAKEFILE) BINS_TO_LOAD=FLYME.DAT BOOTSPOT_DST=$(EVA_SYSTEM) createbootspot

testimages: assets/nsr/nge-ending-moon.hgr assets/nsr/nge-ending-rei.hgr
	$(MAKE) -f $(CURR_MAKEFILE) DISK_TO_MAKE=$(TEST_IMAGE_DISK) DISK_VOLUME_TO_MAKE=TEST.DISK makeblankprodos
	$(AC) -p $(TEST_IMAGE_DISK) moon.hgr bin 0x0000 <assets/nge-ending-moon.hgr
	$(AC) -p $(TEST_IMAGE_DISK) rei.hgr bin 0x0000 <assets/nge-ending-rei.hgr

test:
	-lupinecore_a2/extern/AppleWin/Applewin.exe -no-printscreen-key -d1 $(CURR_PATH)/$(PRODOS_BOOT_DISK) -s7 empty

.PHONY: all clean cleanassets cleandsk test cleantxt mount cleanmusic test copydsk

assets: $(ASSETS_DISK)

all: | bin dsk

dsk: $(DISKIMAGE)

music: $(BUILDDST)/assets/flyme1.inc

cleanmusic:
	-rm -f assets/nsr/musicflyme1.inc $(BUILDDST)/assets/flyme1.bbt

cleanassets: $(ASSETS)
	-rm -f $(ASSETS_DISK) $(ASSETS_INCLUDE) $(TEST_IMAGE_DISK) $(EVA_SYSTEM) $(ASM_STUB_BIN)

cleandsk:
	-rm -f $(DISKIMAGE)

clean: cleanbin cleandsk cleanassets cleanmusic
